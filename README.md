# README #

Per cambiare la struttura dei livelli, modificare il file livelli.txt nella sequenza di entrate secondo la struttura: tempo_in_ms Nome_classe_zombie Lane_di_entrata

Per modificare le piante disponibili, modificare il file utenti.txt aggiungendo o togliendo i nomi delle classi (esatti lettera per lettera) nella sezione aperta con "Piante sbloccate:".

Non aggiungere o togliere caratteri all'infuori di queste specifiche per favore.

Il codice è distribuito con licenza GPLv3.

Non detengo il copyright nè la proprietà intellettuale di nessuna immagine e di nessuna meccanica di questo gioco.