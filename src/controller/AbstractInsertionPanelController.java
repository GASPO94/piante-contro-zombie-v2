package controller;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 *
 * Astratta utile per i pannelli che hanno elementi da inserire e/o rimuovere.
 * Utile solo un po'.
 *
 * @param <X>
 *            Oggetto da inserire
 * @param <Y>
 *            Tipo di pannello associato
 * 
 * @author Martino De Simoni
 */

/*
 * Anche se � il controller di un pannello a inserimento, non esiste la classe
 * InsertionPanel.
 * L'inserimento pu� avvenire in un migliaio di modi (JList, Listener..) e non
 * ha senso creare astratte o interfacce a parte.
 */

public abstract class AbstractInsertionPanelController<X, Y>
	extends AbstractPanelController<Y> {

    /**
     * Insieme di oggetti inseriti.
     */

    protected Set<X> set = new HashSet<>();

    /**
     * Toglie un elemento. Consigliato l'override.
     * 
     * @param delendum
     *            Elemento da rimuovere
     */

    protected void delete(final X delendum) {

	set.remove(delendum);

    }

    /**
     * Aggiunge un elemento. Consigliato l'override.
     * 
     * @param inserendum
     *            Elemento da rimuovere
     */

    public void insert(final X inserendum) {

	set.add(inserendum);

    }

}
