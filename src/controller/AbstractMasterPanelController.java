package controller;

import gui.EventiPannello;
import gui.MainFrame;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Si intende il Master dei PanelController, che intuitivamente gestisce una
 * risorsa di tipo frame.
 * 
 * @author Martino De Simoni
 */

public abstract class AbstractMasterPanelController
	implements IMasterController {

    @SuppressWarnings("rawtypes")
    protected Map<EventiPannello, AbstractPanelController> slaves = new HashMap<>();
    protected MainFrame frame;

}
