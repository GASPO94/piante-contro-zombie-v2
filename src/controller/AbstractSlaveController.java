package controller;

import gui.EventiPannello;

/**
 * Astratta degli slaveController. Ogni slave deve tenere traccia del suo master
 * per comunicare con lui.
 *
 * @author Martino De Simoni
 */

public abstract class AbstractSlaveController implements ISlaveController {

    /**
     * Master del controller slave.
     */
    protected AbstractMasterPanelController master;
    /**
     * Stringa per identificarsi al master.
     */
    protected EventiPannello panelID;
}
