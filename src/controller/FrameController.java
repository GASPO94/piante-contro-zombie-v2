package controller;

import gui.MainFrame;
import gui.PlantButton;
import gui.UserChoicePanel;
import gui.Utility;
import gui.EventiPannello;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JPanel;

import filemanager.Giocatore;
import filemanager.NessunGiocatore;
import filemanager.UserDataManager;
import filemanager.UserDataManagerImpl;

/**
 * 
 * 
 * Controller del frame principale (e unico in questo progetto), e master dei
 * vari panelController.
 * 
 * @author Martino De Simoni
 */
/*
 * Classe disegnata ad hoc e difficilmente riutilizzabile.
 * 
 */

public class FrameController extends AbstractMasterPanelController
	implements Runnable {

    // Oggetti per operare su file

    private Giocatore g = NessunGiocatore.NESSUN_GIOCATORE; // pu� cambiare a
							    // seconda della
							    // scelta utente.
							    // Importante non
							    // mettere questo
							    // campo a final
    private final UserDataManager dataManager;

    /**
     * Inizializza il frame, il dataManager e i livelli.
     */

    public FrameController() {

	frame = new MainFrame(Utility.TITLE, Utility.LOGO);
	dataManager = new UserDataManagerImpl(
		controller.Utility.FILE_DATI_UTENTE); // deve davvero stare qui?

    }

    /**
     * Alias di PlantsVsZombies
     */

    @Override
    public void run() {
	plantsVsZombies();
    }

    /**
     * Inizio del gioco Piante contro Zombie.
     * Ha il ruolo del metodo run nelle altre classi: raccoglie le istruzioni da
     * eseguire all'avvio del controller.
     * Questo metodo � anche un alias di run.
     */

    public void plantsVsZombies() {

	final AbstractPanelController<UserChoicePanel> userChoice = new UserChoiceController(
		dataManager, frame, this);
	slaves.put(EventiPannello.SCELTA_UTENTE, userChoice);

	frame.setMainPanel((JPanel) slaves.get(EventiPannello.SCELTA_UTENTE)
		.getControlledPanel(), true);
	frame.setVisible(true);
	slaves.get(EventiPannello.SCELTA_UTENTE).run();

    }

    /*
     * Ogni blocco che comincia con un if descrive la logica relativa al
     * controller definito nella clausola.
     * Per esempio, nel blocco if (msg==MenuMessage){} si definisce il
     * comportamento del Pannello del men�.
     * 
     * Lascio notare che le stringhe non devono solo fare match, ma devono
     * proprio essere la stessa.
     */
    /**
     * Gli slave notificano al master la loro terminazione, tramite l'ID che il
     * master consegna loro, dai costruttori.
     * 
     * @param msg
     *            ID, passato dal master, del PanelController-slave che richiama
     *            il metodo.
     * @param args
     *            argomenti utili all'esecuzione.
     */
    @Override
    public void notifyMaster(final EventiPannello msg, final Object args) {

	if (msg == EventiPannello.MENU) { // Siamo nel men�

	    // Il secondo argomento � l'id del PanelController a cui si vuole
	    // passare
	    if ((EventiPannello) args == EventiPannello.SCELTA_UTENTE) {

		// cambio di utente: togli tutto
		slaves.clear();
		frame.removeAll();
		this.frame.setVisible(false);
		frame.dispose();
		// ricomincia da capo - easy but expensive way -
		new FrameController().plantsVsZombies();

	    }

	    else if (args == EventiPannello.OPZIONI) {
		// TODO Opzioni ancora non implementate
	    }

	    else if (args == EventiPannello.SCELTA_PIANTE) {

		slaves.get(msg).slaveHasTerminated();

		final PlantChoiceController p = new PlantChoiceController(g,
			this, frame.getSize());
		frame.setMainPanel(p.controlledPanel, true);

		slaves.put((EventiPannello) args, p);
		slaves.get(args).run();

		p.run();

	    }

	    else if (args == EventiPannello.USCITA) {

		dataManager.aggiornaDatiGiocatore(g); // Dovrebbe davvero stare
						      // qui? Il dataManager
						      // deve stare nella classe
						      // di chi assegna il
						      // frame? O i controller
						      // possono inizializzare
						      // una parte del loro
						      // stato?
		System.exit(0);

	    }

	} else if (msg == EventiPannello.SCELTA_UTENTE) {  // Siamo al pannello
							   // di scelta
	    // utente
	    // pu� andare solo al menu, il secondo argomento args � il giocatore
	    // selezionato

	    slaves.get(EventiPannello.SCELTA_UTENTE).slaveHasTerminated();

	    g = (Giocatore) args;

	    final MenuController menu = new MenuController(g, this);
	    slaves.put(EventiPannello.MENU, menu);
	    frame.setMainPanel((JPanel) slaves.get(EventiPannello.MENU)
		    .getControlledPanel(), true);
	    slaves.get(EventiPannello.MENU).run();

	} else if (msg == EventiPannello.SCELTA_PIANTE) { // Siamo alla scelta
							  // delle
	    // piante, si pu� andare solo al
	    // gioco

	    slaves.get(msg).slaveHasTerminated();
	    slaves.remove(msg);

	    @SuppressWarnings("unchecked")
	    final GameController game = new GameController(
		    controller.Utility.FILE_LIVELLI, (List<PlantButton>) args,
		    this, g);
	    slaves.put(EventiPannello.GIOCO, game);
	    frame.setMainPanel((JPanel) slaves.get(EventiPannello.GIOCO)
		    .getControlledPanel(), true);
	    slaves.get(EventiPannello.GIOCO).run();

	} else if (msg == EventiPannello.OPZIONI) {
	    // TODO Opzioni ancora non implementate

	} else if (msg == EventiPannello.GIOCO) {

	    slaves.get(msg).slaveHasTerminated();
	    // slaves.remove(msg);
	   
	    this.dataManager.aggiornaDatiGiocatore(g);
	    // Si passa al men�
	    frame.setMainPanel((JPanel) slaves.get(EventiPannello.MENU)
		    .getControlledPanel(), true);
	    slaves.get(EventiPannello.MENU).run();

	}
    }

    public static void main(final String args[]) {

	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {

		    new FrameController().plantsVsZombies();

		} catch (Exception e) {
		    e.printStackTrace();
		}

	    }
	});

    }

    @Override
    public void notifyController(final EventiPannello selectedMessage) {
	// TODO per ora non c'� bisogno di questo metodo

    }

}
