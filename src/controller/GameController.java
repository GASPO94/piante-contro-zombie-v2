package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;

import esseri.Attacco;
import esseri.AttaccoConFeedback;
import esseri.AbstractAzione;
import esseri.Creazione;
import esseri.IEssere;
import esseri.Movimento;
import esseri.MovimentoPallino;
import esseri.NessunAzione;
import esseri.NessunEssere;
import esseri.Pallino;
import esseri.Pianta;
import esseri.Posizione2D;
import esseri.Quadratino;
import esseri.TipoEssere;
import esseri.TipoTerreno;
import filemanager.Giocatore;
import filemanager.LevelReaderImpl;
import filemanager.Livello;
import esseri.AbstractZombi;
import gui.EventiPannello;
import gui.GamePanel;
import gui.GamePanelFirstImpl;
import gui.PlantButton;

/**
 * 
 * Controller del pannello di gioco. Le piante sono gi� state selezionate.
 * 
 * @author Martino De Simoni
 *
 */

@SuppressWarnings("unused")
public class GameController
	extends AbstractInsertionPanelController<AbstractAzione, GamePanel>
	implements ActionListener, IGameController {

    /*
     * Per adesso cols e rows sono impostati a valori di default. In una
     * versione pi� sofisticata del gioco, il valore � specificato nel livello,
     * e non � final.
     */
    private final static Integer COLS = Utility.DEFAULT_COLS_QUANTITY;
    private final static Integer ROWS = Utility.DEFAULT_ROWS_QUANTITY;

    private final Giocatore g;
    private final Quadratino[][] zolle = new Quadratino[ROWS][COLS];
    private final Pallino[][] pallini = new Pallino[ROWS][COLS];
    private final Livello livello;
    private Timer timer;

    private EventiPannello actionType;
    private String piantaDaCreare;

    private boolean[] tosaerbaMangiato = new boolean[ROWS];
    private boolean perso;
    private boolean vinto;

    private int soli = Utility.SOLI_A_INIZIO_LIVELLO;

    private int zombieRimasti;
    private int timeSinceStart;

    public static Integer getCols() {
	return COLS;
    }

    public static Integer getRows() {
	return ROWS;
    }

    public GameController(final String filePathDatiLivelli,
	    final List<PlantButton> plantButtons,
	    final AbstractMasterPanelController master, final Giocatore g) {

	livello = new LevelReaderImpl(filePathDatiLivelli).leggiDatiLivelli()
		.get(g.getLevel());

	this.master = master;

	this.g = g;

	this.panelID = EventiPannello.GIOCO;

	final List<BufferedImage> immaginiTerreno = new ArrayList<>();
	// TODO cambiare
	for (int i = 0; i <= 1; i++) {
	    for (int j = 0; j < COLS; j++) {
		zolle[i][j] = new Quadratino(TipoTerreno.CORTILE,
			gui.Utility.ERBA); // Solo il livello del cortile per
					   // adesso
		zolle[i][j].setBeing(NessunEssere.getInstance());
		immaginiTerreno.add(zolle[i][j].getImg());
	    }
	}

	// Una riga di acqua
	for (int i = 2; i <= 2; i++) {
	    for (int j = 0; j < COLS; j++) {
		zolle[i][j] = new Quadratino(TipoTerreno.ACQUA,
			gui.Utility.ACQUA);
		zolle[i][j].setBeing(NessunEssere.getInstance());
		immaginiTerreno.add(zolle[i][j].getImg());
	    }
	}

	// E le altre di erba
	for (int i = 3; i < ROWS; i++) {
	    for (int j = 0; j < COLS; j++) {
		zolle[i][j] = new Quadratino(TipoTerreno.CORTILE,
			gui.Utility.ERBA); // Solo il livello del cortile per
					   // adesso
		zolle[i][j].setBeing(NessunEssere.getInstance());
		immaginiTerreno.add(zolle[i][j].getImg());
	    }
	}

	controlledPanel = new GamePanelFirstImpl(plantButtons, COLS, ROWS, this,
		immaginiTerreno);

    }

    @Override
    public void slaveHasTerminated() {

	this.controlledPanel.setVisible(false);
    }

    public void insert(final AbstractAzione[] inserendum) {
	for (final AbstractAzione a : inserendum) {
	    super.insert(a);
	}

    }

    /*
     * Inadatto a questa classe
     * 
     * @see controller.IController#notifyController(gui.EventiPannello)
     */
    @Deprecated
    @Override
    public void notifyController(final EventiPannello msg) {

    }

    protected void esaurisciAttaccoConFeedback(
	    final AttaccoConFeedback attacco) {

	final int x = (int) attacco.doveAttaccare.getX();
	final int y = (int) attacco.doveAttaccare.getY();

	if (areLegalCoordinates(x, y) && zolle[y][x].getContainedBeing()
		.getTipoEssere() == attacco.chiAttaccare) { // l'attacco andr� a
							    // buon fine
	    attacco.attaccante.riceviFeedback(true);

	}
	esaurisciAttacco((Attacco) attacco);

    }

    protected void esaurisciAttacco(final Attacco attacco) {

	final int x = (int) attacco.doveAttaccare.getX();
	final int y = (int) attacco.doveAttaccare.getY();

	if (areLegalCoordinates(x, y) && zolle[y][x].getContainedBeing()
		.getTipoEssere() == attacco.chiAttaccare) {

	    zolle[y][x].getContainedBeing()
		    .prendiDanno(attacco.danno.doubleValue());

	    if (zolle[y][x].getContainedBeing().isDead()) {

		zolle[y][x].getContainedBeing().eseguireAllaMorte();
		eraseSquare(zolle[y][x]);

	    }

	} else if(areLegalCoordinates(x, y) && attacco.chiAttaccare == TipoEssere.PALLINO){ //Il pallino non ha vita, deve morire subito
	    zolle[y][x].setPea(null);
	} else {
	    esaudisciRichiesta(attacco.inCasoDiFallimento);
	}
    }

    /**
     * Esaurimento di una richiesa di movimento in orizzontale o verticale
     */
    protected void esaurisciMovimento(final Movimento movimento) {

	final int x = (int) movimento.doveSpostarsi.getX();
	final int y = (int) movimento.doveSpostarsi.getY();
	final IEssere chiSiMuove = movimento.chiSiMuove;

	if (areLegalCoordinates(x, y) && zolle[y][x].canContain(chiSiMuove)) {

	    zolle[y][x].setBeing(chiSiMuove);
	    eraseSquare(zolle[(int) movimento.posizioneDiChiSiMuove
		    .getY()][(int) movimento.posizioneDiChiSiMuove.getX()]);

	} else if (movimento.chiSiMuove instanceof AbstractZombi && x < 0) { // uno
	    // zombie arriva in fondo e si mangia un tosaerba

	    if (tosaerbaMangiato[(int) movimento.doveSpostarsi.getY()]) {
		perso = true; // Tosaerba gi� mangiato
	    } else {
		tosaerbaMangiato[(int) movimento.doveSpostarsi.getY()] = true; // Gnam
	    }
	    eraseSquare(zolle[(int) movimento.posizioneDiChiSiMuove
		    .getY()][(int) movimento.posizioneDiChiSiMuove.getX()]);
	    // Lo zombie esce di scena
	    zombieRimasti--; // La chiamata di prima non aggiorna
			     // zombieRimasti, perch� lo zombi non � morto
	} else {
	    esaudisciRichiesta(movimento.inCasoDiFallimento);
	}
    }

    protected void esaurisciMovimentoPallino(final MovimentoPallino movimento) {

	final int x = (int) movimento.doveSpostarsi.getX();
	final int y = (int) movimento.doveSpostarsi.getY();
	final Pallino chiSiMuove = (Pallino) movimento.chiSiMuove;
	if (areLegalCoordinates(x, y) && zolle[y][x].canContain(chiSiMuove)) {

	    zolle[y][x].setPea(chiSiMuove);
	    zolle[(int) movimento.posizioneDiChiSiMuove
		    .getY()][(int) movimento.posizioneDiChiSiMuove.getX()]
			    .setPea(null);

	} else if (x >= COLS - esseri.Utility.QUADRETTO_DW.getX()) {
	    zolle[(int) movimento.posizioneDiChiSiMuove
		    .getY()][(int) movimento.posizioneDiChiSiMuove.getX()]
			    .setPea(null);
	} else {
	    esaudisciRichiesta(movimento.inCasoDiFallimento);
	}
    }

    protected void esaurisciCreazione(final Creazione creazione) {

	final int x = (int) creazione.doveCreare.getX();
	final int y = (int) creazione.doveCreare.getY();
	//System.out.println(creazione.cosaCreare.getClass());
	//System.out.println(areLegalCoordinates(x, y));
	//System.out.println(x + "  " + y);
	if (areLegalCoordinates(x, y)
		&& zolle[y][x].canContain(creazione.cosaCreare)) {
	   //System.out.println("pu�");
	    if (creazione.cosaCreare instanceof Pallino) {
		 zolle[y][x].setPea((Pallino) creazione.cosaCreare);
	    } else {
		zolle[y][x].setBeing((IEssere) creazione.cosaCreare);
	    }
	} else {
	    if(creazione.cosaCreare.getTipoEssere()== TipoEssere.ZOMBIE){
		zombieRimasti--;
	    }
	    esaudisciRichiesta(creazione.inCasoDiFallimento);
	}

    }

    /**
     * 
     * Esaudisce la richiesta espressa tramite l'azione inserita come parametro.
     * esseriSenzienti e Pallini devono avere taglia uguale.
     * 
     * @param azioneGenerica
     * @param zolle
     * @param pallini
     */
    /*
     * Per implementare i minigiochi, � pi� facile sovrascrivere questa funzione
     * che le subordinate rispettive
     */
    protected void esaudisciRichiesta(final AbstractAzione azioneGenerica) {

	if (azioneGenerica instanceof AttaccoConFeedback) {
	    esaurisciAttaccoConFeedback((AttaccoConFeedback) azioneGenerica);
	} else if (azioneGenerica instanceof MovimentoPallino) {
	    esaurisciMovimentoPallino((MovimentoPallino) azioneGenerica);
	} else if (azioneGenerica instanceof Attacco) {
	    esaurisciAttacco((Attacco) azioneGenerica);
	} else if (azioneGenerica instanceof Movimento) {
	    esaurisciMovimento((Movimento) azioneGenerica);
	} else if (azioneGenerica instanceof Creazione) {
	    esaurisciCreazione((Creazione) azioneGenerica);
	}

    }

    private void esaudisciRichieste() {

	for (final AbstractAzione azioneGenerica : this.set) {

	    esaudisciRichiesta(azioneGenerica);

	}

	set.clear(); // Tutte le azioni sono state eseguite
    }

    @Override
    public void run() {

	this.controlledPanel.setVisible(true);

	zombieRimasti = livello.numeroZombie;
        //System.out.println("Zombie iniziali:" + zombieRimasti);
	timer = new Timer(Utility.TICK, this);
	timer.setRepeats(true);
	timer.setInitialDelay(0);
	timer.start();
    }

    protected void eraseSquare(final Quadratino q) {

	if (q.getContainedBeing().isDead()
		&& q.getContainedBeing() instanceof AbstractZombi) {
	    zombieRimasti--;
	 //    System.out.println("Zombie ucciso. Zombie rimasti: " +
	 //    zombieRimasti);
	}
	if (zombieRimasti <= 0 && !perso) {
	    vinto = true;
	}
	q.makeEmpty();

    }

    protected boolean areLegalCoordinates(final int x, final int y) {
	//System.out.println("chiamata a legal coor");
	//System.out.println("X: "+x +" Y: "+y);
	//System.out.println("Righe: "+ROWS + " Colonne: "+COLS);
	//System.out.println(y < ROWS && y >= 0 && x < COLS && x >= 0);
	return y < ROWS && y >= 0 && x < COLS && x >= 0;
    }

    private void updatePlayerData(final Giocatore g, final boolean vittoria) {

	// TODO cambiare il livello, mettere piante disponibili in pi� se
	// bisogna..
	// Metodo vuoto per questa implementazione con un solo livello.
	if (vittoria) {
	    switch (g.getLevel()) { // Lascio l'if, in futuro ci saranno pi�
				    // livelli
	    case "1":
		g.setLevel("2");
		break;
	    default:
		break;
	    }
	}
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {

	List<BufferedImage> immagini = new ArrayList<>(COLS * ROWS);

	/*
	 * Per ogni partita: All'inizio: Metto gli zombi all'inizio se devo
	 * Aspetto un tick Per ogni turno successivo: Metto gli zombi se ce ne
	 * sono Accolgo le richieste degli esseri se ce ne sono Aggiorno la GUI
	 * Aspetto un tick
	 * 
	 * Intanto: accolgo le richieste della GUI
	 */

	if (!vinto && !perso) {

	    final int startTurnTime = (int) System.currentTimeMillis();
	    boolean zombieNonAggiunto;
	    immagini = new ArrayList<>();
	    for (int y = 0; y < ROWS; y++) {
		for (int x = 0; x < COLS; x++) {
		    insert(zolle[y][x].getContainedBeing()
			    .inAzione(Utility.TICK, new Posizione2D(x, y)));
		    if (zolle[y][x].hasPea()) {
			insert(zolle[y][x].getContainedPea()
				.inAzione(Utility.TICK, new Posizione2D(x, y)));
		    }
		}
	    }

	    do { // metti altri zombie se � il momento

		if (!livello.zombie.isEmpty()
			&& livello.zombie.get(0).tempoInMs <= timeSinceStart) {

		    try {
			@SuppressWarnings("unchecked")
			final Class<? extends AbstractZombi> nuovoZombie = (Class<? extends AbstractZombi>) Class
				.forName("esseri."
					+ livello.zombie.get(0).nomeZombie);
			
			    this.insert(new Creazione((AbstractZombi)nuovoZombie.newInstance(),
				    NessunAzione.getInstance(),
				   new Posizione2D(COLS-1,livello.zombie.get(0).laneId -1)));
				  
			// System.out.println("entrato "
			// + livello.zombie.get(0).nomeZombie);
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    livello.zombie.remove(0);
		    zombieNonAggiunto = false;
		}

		else {
		    zombieNonAggiunto = true;
		}
	    } while (!zombieNonAggiunto);

	    esaudisciRichieste();
	    // Aggiorno la GUI
	    int i = 0;
	    for (int y = 0; y < ROWS; y++) {
		for (int x = 0; x < COLS; x++) {
		    if (zolle[y][x].isOccupiedByBeing()) {
			immagini.add(i,
				zolle[y][x].getContainedBeing().getImg());
		    } else if (zolle[y][x].hasPea()) {
			immagini.add(i, zolle[y][x].getContainedPea().getImg());
		    } else {
			immagini.add(i,
				zolle[y][x].getContainedBeing().getImg());
		    }

		    i++;
		}
	    }

	    this.controlledPanel.updateImmaginiPiante(immagini, this.soli);

	    final int endTurnTime = (int) System.currentTimeMillis();
	    final int turnTime = endTurnTime - startTurnTime;

	    timeSinceStart += Utility.TICK;// C'abbiamo messo un tick,
					   // idealmente. Facciamo finta di
					   // niente nel remoto caso
					   // contrario mi sembra il
					   // miglior modo di garantire il
					   // buon gameplay
	} else {
	    timer.setRepeats(false);
	    timer.stop();
	    vinto = !perso; // Questo protegge il codice nel caso particolare in
			    // cui L'ULTIMO ZOMBIE arrivi in fondo a tosaerba
			    // gi� mangiato, quindi il programma segna sia che
			    // si � vinto che si � perso. Il codice � protetto
			    // anche in altri modi.
	    updatePlayerData(g, vinto);
	    if (vinto) {
		this.controlledPanel.inviaMessaggioVittoria();
	    } else {
		this.controlledPanel.inviaMessaggioSconfitta();
	    }
	    this.master.notifyMaster(this.panelID, vinto);

	}
    }

    @Override
    public void clickPala() {
	this.actionType = EventiPannello.SELEZIONE_PALA;

    }

    @Override
    public void clickBottone(final String plantName) {
	this.actionType = EventiPannello.SELEZIONE_PIANTA;
	this.piantaDaCreare = plantName;
    }

    @Override
    public void clickGriglia(final Posizione2D pos) {
	final int x = (int) pos.getX();
	final int y = (int) pos.getY();
	if (actionType == EventiPannello.SELEZIONE_PIANTA) { // Click sul
							     // bottone

	    try {
		@SuppressWarnings("unchecked")
		final Class<? extends Pianta> nuovaPianta = (Class<? extends Pianta>) Class
			.forName("esseri." + piantaDaCreare);

		final Pianta pianta = (Pianta) nuovaPianta.newInstance();
		if (pianta.getSoliRichiesti() <= this.soli
			&& areLegalCoordinates(x, y)) {
		    this.insert(new Creazione(
			    (Pianta) nuovaPianta.newInstance(),
			    NessunAzione.getInstance(), new Posizione2D(x, y)));
		    // zolle[y][x].setBeing((Pianta) nuovaPianta.newInstance());
		    soli -= pianta.getSoliRichiesti();
		}

	    } catch (Exception e) {
		e.printStackTrace();
	    }

	} else if (actionType == EventiPannello.SELEZIONE_PALA && zolle[y][x]
		.getContainedBeing().getTipoEssere() == TipoEssere.PIANTA) {
	    eraseSquare(zolle[y][x]);
	}
    }

}
