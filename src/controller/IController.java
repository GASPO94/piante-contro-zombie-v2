package controller;

import gui.EventiPannello;

/**
 * 
 * 
 *
 * Interfaccia per i Controller.
 *
 * @author Martino De Simoni
 *
 */

/*
 * Mi vengono in mente solo esempi di controller di elementi di view, quindi �
 * difficile non estendere qui l'interfaccia runnable.
 * Una scelta da discutere.
 * 
 * Nelle classi che sicuramente devono disporre di un metodo run, runnable �
 * stata estesa di nuovo. Nel caso l'estensione di questa
 * classe venga eliminata, il programma non risentir� di errori di compilazione.
 */

public interface IController extends Runnable {

    /**
     * Metodo richiamato sul controller per notificargli l'espletazione della
     * funzione dell'oggetto controllato.
     * Pu� essere chiamato dall'oggetto controllato o dal controller stesso.
     * Il significato degli argomenti deve essere fornito a priori nella
     * javadoc.
     * 
     * @param selectedMessage
     *            messaggio utile all'esecuzione.
     * 
     */

    void notifyController(EventiPannello selectedMessage);

}
