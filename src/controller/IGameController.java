package controller;

import esseri.Posizione2D;

public interface IGameController {

    void clickPala();

    void clickBottone(String plantName);

    void clickGriglia(Posizione2D posizione);
}
