package controller;

import filemanager.Giocatore;
import gui.EventiPannello;
import gui.MenuPanel;

/**
 * 
 * 
 * Controller del pannello del men�.
 * 
 * @author Martino De Simoni
 */
/*
 * 
 * La classe implementa il pattern mvc e compone la view.
 * La presente � una classe totalmente "stupida", che definisce la sola grafica
 * relegando al controller quasi qualsiasi azione
 * di logica.
 *
 * Nonostante sia stata disegnata ad hoc, la classe � in buona parte
 * riutilizzabile.
 *
 */

public class MenuController extends AbstractPanelController<MenuPanel> {

    /**
     * Inizializza lo stato del controller.
     * 
     * @param giocatore
     *            Il giocatore di cui vengono visualizzati i dati
     * @param menu
     *            La stringa da dare come argomento a notifyMaster() per
     *            notificare la propria terminazione.
     * @param sceltaPiante
     *            Stringa che il pannello associato ritorner� a
     *            notifyController(). Il controller notificher� al master.
     * @param opzioni
     *            Stringa che il pannello associato ritorner� a
     *            notifyController(). Il controller notificher� al master.
     * @param sceltaPiante2
     *            Stringa che il pannello associato ritorner� a
     *            notifyController(). Il controller notificher� al master.
     * @param uscita
     *            Stringa che il pannello associato ritorner� a
     *            notifyController(). Il controller notificher� al master.
     * @param master
     *            Il master (consiglio: se si usa il costruttore da master,
     *            usare "this")
     */

    public MenuController(final Giocatore giocatore,
	    final AbstractMasterPanelController master) {

	this.panelID = EventiPannello.MENU;

	this.master = master;

	controlledPanel = new MenuPanel(this, giocatore, gui.Utility.SFONDO);

    }

    @Override
    public void slaveHasTerminated() {

	this.controlledPanel.setVisible(false); // questo pannello resta in
					        // memoria

    }

    @Override
    public void notifyController(final EventiPannello msg) {

	if (msg == EventiPannello.SCELTA_PIANTE) {

	    master.notifyMaster(panelID, EventiPannello.SCELTA_PIANTE);

	} else if (msg == EventiPannello.OPZIONI) {

	    master.notifyMaster(panelID, EventiPannello.OPZIONI);

	} else if (msg == EventiPannello.SCELTA_UTENTE) {

	    master.notifyMaster(panelID, EventiPannello.SCELTA_UTENTE);

	} else if (msg == EventiPannello.USCITA) {

	    master.notifyMaster(panelID, EventiPannello.USCITA);
	}

    }

    @Override
    public void run() {

	this.controlledPanel.setVisible(true);
	this.controlledPanel.update(this.controlledPanel.getGraphics());

    }

}
