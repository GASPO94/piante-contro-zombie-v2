package controller;

import java.awt.Dimension;
import java.util.HashSet;

import javax.swing.ImageIcon;

import esseri.Pianta;
import filemanager.Giocatore;
import gui.EventiPannello;
import gui.PlantButton;
import gui.PlantChoicePanel;

/**
 * 
 * 
 * Controller del pannello della scelta delle piante da utilizzare.
 * 
 * @author Martino De Simoni
 * 
 */
/*
 * La classe implementa il pattern mvc e il controller.
 *
 * La classe � difficilmente riutilizzabile. Sarebbe pi� semplice ricominciare
 * da capo e prendere a modello gli altri controller.
 *
 */
// BUGGY riga 49 circa: bottoni inizializzati per debug
public class PlantChoiceController
	extends AbstractPanelController<PlantChoicePanel> {

    final private static int MAX_PLANT = 4;

    /**
     * 
     * @param gioco
     *            Stringa che il pannello associato ritorner� a
     *            notifyController(). Il controller notificher� al master.
     * @param _g
     *            Il giocatore che sceglie le proprie piante.
     * @param _master
     *            Il master di questo controller.
     * @param maxSize
     *            Dimensione massima del frame.
     */

    public PlantChoiceController(final Giocatore giocatore,
	    final AbstractMasterPanelController _master,
	    final Dimension maxSize) {

	master = _master;

	// Inizializzare i plantbuttons e il panel
	final HashSet<PlantButton> bottoni = new HashSet<>();

	for (final String p : giocatore.pianteSbloccate) {
	    try {

		@SuppressWarnings("unchecked")
		final Class<? extends Pianta> nuovaPianta = (Class<? extends Pianta>) Class
			.forName("esseri." + p);
		final Pianta pianta = (Pianta) nuovaPianta.newInstance();
		bottoni.add(new PlantButton(new ImageIcon(pianta.getImg()),
			pianta.getSoliRichiesti(), p));

	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}

	controlledPanel = new PlantChoicePanel(bottoni, gui.Utility.SFONDO,
		this, MAX_PLANT, maxSize);

    }

    @Override
    public void slaveHasTerminated() {

	this.controlledPanel.setVisible(false);

    }

    @Override
    public void notifyController(final EventiPannello msg) {

	if (msg == EventiPannello.GIOCO
		&& controlledPanel.getChosenPlantSize() == MAX_PLANT) {

	    this.master.notifyMaster(EventiPannello.SCELTA_PIANTE,
		    controlledPanel.getChosenPlants());

	}
    }

    @Override
    public void run() {

	this.controlledPanel.setVisible(true);

    }

}
