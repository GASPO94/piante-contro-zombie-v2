package esseri;

/**
 * 
 * Le azioni sono lo strumento il modo tramite il quale gli enti del gioco
 * effettivo comunicano con il controller.
 *
 * @author Martino De Simoni
 */

public abstract class AbstractAzione {

    // TODO animazioni
    public final AbstractAzione inCasoDiFallimento; // Se la prima azione
						    // fallisce, si
    // esegue questo campo.

    public AbstractAzione(final AbstractAzione incasodifallimento) {

	this.inCasoDiFallimento = incasodifallimento;
    }

    public AbstractAzione() {

	this.inCasoDiFallimento = NessunAzione.getInstance();

    }

}