package esseri;

/**
 * @author Andrea
 * 
 * 
 *         Consumabili � una classe astratta che contiene i metodi usabili nelle
 *         classi che la estendono (esempio CiliegieEsplosive)
 *
 */
// classe astratta che implementa i metodi delle piante consumabili del
// programma
public abstract class AbstractConsumabile extends Pianta {

    protected boolean hoAttaccato;

    public AbstractConsumabile(final String path, final Double life,
	    final Double danno, final TipoTerreno terreno, final int costo) {

	super(path, life, danno, Utility.PIANTA_TEMPO_BASSO, terreno,costo);

    }
    
}