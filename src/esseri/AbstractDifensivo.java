package esseri;

/**
 * @author Andrea
 * 
 *         Difensivo � una classe astratta che contiene metodi usabili nelle
 *         classi che la estendono (es. Noce) ======= Difensivo è una classe
 *         astratta che contiene metodi usabili nelle classi che la estendono
 *         (es. Noce) >>>>>>> refs/remotes/default/master
 *
 */
// classe astratta difensivo che implementa i metodi delle piante difensive del
// programma
public abstract class AbstractDifensivo extends Pianta {

    public AbstractDifensivo(final String path, final Double life,
	    final Double danno, final Integer tempoRichiesto,
	    final TipoTerreno terreno, final int costo) {

	super(path, life, danno, tempoRichiesto, terreno,costo);
    }

}
