/**
 * 
 */
package esseri;

import java.awt.image.BufferedImage;

/**
 * @author Andrea
 * @author Marco
 * @author Martino
 */
public abstract class AbstractEssere implements IEssere {

    protected int tempoTrascorso;
    protected Integer tempoRichiesto;
    protected double life;
    public final TipoEssere nome;
    protected final Double danno;
    protected BufferedImage img;
    protected final TipoTerreno terreno;
    // Costruttori

    public AbstractEssere(final String path, final TipoEssere nome,
	    final Double life, final Double danno, final Integer tempoRichiesto,
	    final TipoTerreno terreno) {

	this.nome = nome;
	this.terreno = terreno;
	this.img = gui.Utility.initImg(path);

	this.life = new Double(life.doubleValue());
	this.danno = new Double(danno.doubleValue());
	this.tempoRichiesto = Integer.valueOf(tempoRichiesto.intValue());
    }

    protected boolean canAct(final int tempo) { // side effect su tempo_in_ms

	this.tempoTrascorso += tempo;
	if (tempoTrascorso >= tempoRichiesto) {
	    this.tempoTrascorso -= tempoRichiesto;
	    return true;
	}
	return false;
    }

    public double getLife() {
	return this.life;
    }

    public int getTempoTrascorso() {
	return tempoTrascorso;
    }

    public int getTempoRichiesto() {
	return tempoRichiesto;
    }

    public TipoEssere getTipoEssere() {
	return this.nome;
    }

    @Override
    public BufferedImage getImg() {
	return img;
    }

    public TipoTerreno getTerreno() {

	return this.terreno;

    }

    protected void cambiaImmagine(final String path) {
	img = gui.Utility.initImg(path);
    }

}
