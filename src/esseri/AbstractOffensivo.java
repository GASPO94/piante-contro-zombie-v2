package esseri;

/**
 * @author Andrea L'astratta Offensivo implementa i metodi utilizzati dalle
 *         pianti che la estendono
 *
 */
// classe astratta offensivo che implementa i metodi per tutte le piante
// offensive del programma
public abstract class AbstractOffensivo extends Pianta {

    public AbstractOffensivo(final String path, final Double life, Double danno,
	    final Integer tempoRichiesto, final TipoTerreno terreno,final int costo) {
	super(path, life, danno, tempoRichiesto, terreno,costo);
    }

}