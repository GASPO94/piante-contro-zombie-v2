package esseri;

/**
 *  Support � un'astrata che contiene metodi per le piante di
 *  supporto.
 *
 * @author Andrea
 */
// astratta supporto che implementa i metodi delle piante di supporto del
// programma
public abstract class AbstractSupport extends Pianta {

    boolean hoAttaccato = false;

    public AbstractSupport(final String path, final Double life, final Double danno,
	    final Integer tempoRichiesto, final TipoTerreno terreno,final int costo) {

	super(path, life, danno, tempoRichiesto, terreno,costo);
    }
}