package esseri;

/*classe astratta che mi definisce tutte le caratteristiche comuni a tutti gl zombi
 *  
 * 
 */

/**
 * Astratta di ogni zombi
 * 
 * @author Martino De Simoni
 * @author Marco Martini
 */

public abstract class AbstractZombi extends AbstractEssere {

    // Setto il tipoTerreno a cortile tanto la maggior parte delgi zombi è li
    // per gli zombi che non sono
    // del cortile utilizzo lo implemento nelle classi concrete degli zombi

    protected static final String CARTELLA_IMMAGINI_ZOMBI = "imgZombi/"; // infallibile

    public AbstractZombi(final String imgName, final Double life,
	    final Double danno, final Integer tempoRichiesto,
	    final TipoTerreno terreno) {

	super(CARTELLA_IMMAGINI_ZOMBI + imgName, TipoEssere.ZOMBIE, life, danno,
		tempoRichiesto, terreno);
    }

    public void prendiDanno(final double danno) {

	super.life -= danno;

    }

    // algoritmo di fai robe da finire

    public double getLife() {

	return this.life;
    }

    public double getDanno() {

	return super.danno;
    }

    public void eseguireAllaMorte() {
    }

    public boolean isDead() {

	return this.life <= 0;
    }

}
