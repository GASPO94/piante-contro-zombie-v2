/**
 * 
 */
/**
 * 
 */
package esseri;

/**
 * Questa pianta , � un'alga acquatica, che pu� essere giocata
 * solo in acqua , e una volta messa in gioco non fa nulla se non �
 * attaccata, ma quando viene attaccata, infligge un danno elevato allo
 * zombie.
 *
 * @author Andrea
 */
public class AlgaKombu extends AbstractSupport implements RicevitoreDiFeedback {

    private static String imgFilePath = "algakombuTrasparente.png";
    public final static int COSTO = 75;

    private static double vitaMax = Utility.PIANTA_VITA_BASSA;

    public AlgaKombu() {

	super(imgFilePath, vitaMax, Utility.PIANTA_DANNO_LETALE,
		Utility.PIANTA_TEMPO_MINIMO, TipoTerreno.ACQUA,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {
	if (canAct(tempoTrascorso)) {
	    if (hoAttaccato) {
		final AbstractAzione suicidio = new Attacco(TipoEssere.PIANTA,
			Utility.QUADRETTO_CORRENTE, this.life,
			NessunAzione.getInstance(), posizione);
		return new AbstractAzione[] { suicidio };

	    } else {
		final AbstractAzione attaccoDietro = new AttaccoConFeedback(
			this, TipoEssere.ZOMBIE, Utility.QUADRETTO_SX, danno,
			NessunAzione.getInstance(), posizione);
		final AbstractAzione attaccoDavanti = new AttaccoConFeedback(
			this, TipoEssere.ZOMBIE, Utility.QUADRETTO_DX, danno,
			attaccoDietro, posizione);

		return new AbstractAzione[] { attaccoDavanti };
	    }
	}
	return new AbstractAzione[0];
    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.ACQUA;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public void riceviFeedback(final boolean risultatoAttacco) {

	hoAttaccato = risultatoAttacco;

    }

}
