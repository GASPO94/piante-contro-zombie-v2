package esseri;

/**
 * 
 *
 * Azione di attacco comunicata dagli enti al controller.
 *
 * @author Martino De Simoni
 *
 */

public class Attacco extends AbstractAzione {

    public final TipoEssere chiAttaccare; // Piante? Zombie? Pallini? Uso le
					  // enum perchÄ� sono piÅ facili da
					  // usare.
    public final Posizione2D distanzaDaAttaccante;
    public final Posizione2D posizioneAttaccante;
    public final Posizione2D doveAttaccare;

    public final Double danno;

    public Attacco(final TipoEssere chiAttaccare,
	    final Posizione2D distanzaDaAttaccante, final double danno,
	    final AbstractAzione inCasoDiFallimento,
	    final Posizione2D posizioneAttaccante) {
	super(inCasoDiFallimento);
	this.chiAttaccare = chiAttaccare;
	this.distanzaDaAttaccante = distanzaDaAttaccante;
	this.danno = danno;
	this.posizioneAttaccante = posizioneAttaccante;

	this.doveAttaccare = posizioneAttaccante
		.sumPositions(distanzaDaAttaccante);
    }

}
