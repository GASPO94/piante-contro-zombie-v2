package esseri;

public class AttaccoConFeedback extends Attacco {
    public final RicevitoreDiFeedback attaccante;

    public AttaccoConFeedback(final RicevitoreDiFeedback attaccante,
	    final TipoEssere chiAttaccare,
	    final Posizione2D distanzaDaAttaccante, final double danno,
	    final AbstractAzione inCasoDiFallimento,
	    final Posizione2D posizioneAttaccante) {
	super(chiAttaccare, distanzaDaAttaccante, danno, inCasoDiFallimento,
		posizioneAttaccante);
	this.attaccante = attaccante;
    }
}
