/**
 * 
 */
package esseri;

/**
 *
 * CiliegieEsplosive ha un attacco elevato e colpisce gli zombie vicini
 * , come mostra il metodo faiRobe
 *
 * @author Andrea
 *
 */
public class CiliegieEsplosive extends AbstractConsumabile {

    private static String imgFilePath = "ciliegieesplosiveTrasparenti.png";
    public final static int COSTO = 250;
    private boolean attaccato;

    public CiliegieEsplosive() {

	super(imgFilePath, Utility.PIANTA_VITA_BASSA, Utility.PIANTA_DANNO_ALTO,
		TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    if (attaccato) {
		return new AbstractAzione[] {

			new Attacco(TipoEssere.PIANTA,
				Utility.QUADRETTO_CORRENTE, this.getLife(),
				NessunAzione.getInstance(), posizione) };
	    } else {
		attaccato = true;
		return new AbstractAzione[] {

			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW_SX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_SX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP_SX,
				danno, NessunAzione.getInstance(), posizione) };

	    }
	}
	return new AbstractAzione[0];
    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
