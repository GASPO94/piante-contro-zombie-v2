package esseri;

/**
 * 
 * Comunica la creazione di un pallino, di un altro zombie..
 *
 * @author Martino De Simoni
 *
 */

/*
 * Non ci sono meccanismi di sicurezza per impedire al programmatore di far
 * creare a una pianta uno zombie.
 */

public class Creazione extends AbstractAzione {

    public final IEssere cosaCreare;
    public final Posizione2D doveCreare;
    public final Posizione2D distanzaDaCreatore;
    public final Posizione2D posizioneCreatore;

    public Creazione(final IEssere essere,
	    final AbstractAzione incasodifallimento,
	    final Posizione2D posizioneCreatore,
	    final Posizione2D distanzaDaCreatore) {

	super(incasodifallimento);
	this.posizioneCreatore = posizioneCreatore;
	this.distanzaDaCreatore = distanzaDaCreatore;
	this.doveCreare = posizioneCreatore.sumPositions(distanzaDaCreatore);
	this.cosaCreare = essere;
    }

    public Creazione(final AbstractEssere essere,
	    final AbstractAzione incasodifallimento,
	    final Posizione2D posizioneCreazione) {

	this(essere, incasodifallimento, new Posizione2D(0, 0),
		posizioneCreazione);

    }

}