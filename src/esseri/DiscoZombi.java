
package esseri;

/*Lo zombi disco ha la peculiarita che quando viene evocato crea 4 zombiBallerini nel campo. 
 * 
 */

/*
 * @author Marco
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class DiscoZombi extends AbstractZombi {

    private boolean inCampo = false;
    private static String imgFilePath = "zombi_disco.jpeg";

    public DiscoZombi() {

	super(imgFilePath, Utility.ZOMBIE_VITA_BASSA,
		Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO,
		TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (this.inCampo == false) {

	    this.inCampo = true;
	    return new AbstractAzione[] {
		    new Creazione(new ZombiBallerino(),
			    NessunAzione.getInstance(), posizione,
			    Utility.QUADRETTO_DW),
		    new Creazione(new ZombiBallerino(),
			    NessunAzione.getInstance(), posizione,
			    Utility.QUADRETTO_DX),
		    new Creazione(new ZombiBallerino(),
			    NessunAzione.getInstance(), posizione,
			    Utility.QUADRETTO_UP),
		    new Creazione(new ZombiBallerino(),
			    NessunAzione.getInstance(), posizione,
			    Utility.QUADRETTO_SX) };

	}
	if (canAct(tempoTrascorso)) {
	    return new AbstractAzione[] { new Attacco(TipoEssere.PIANTA,
		    new Posizione2D(-1, 0), this.danno,
		    new Movimento(new Posizione2D(-1, 0), posizione, this,
			    NessunAzione.getInstance()),

		    posizione) };
	}
	return new AbstractAzione[0];
    }

    public boolean isInCampo() {

	return this.inCampo;

    }

}
