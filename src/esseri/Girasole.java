/**
 * 
 */
package esseri;

/**
 * @author Andrea
 *
 *         Sostanzialmente questa classe Girasole non fa nulla (non attacca )
 *         per questo nel metodo fai robe ho implementato
 *         controller.insert(nessunAzione)
 */
public class Girasole extends AbstractSupport {

    private static String imgFilePath = "girasoleTrasparente.png";
    public final static int COSTO = 100;

    public Girasole() {

	super(imgFilePath, Utility.PIANTA_VITA_MEDIA,
		Utility.PIANTA_DANNO_NULLO, Utility.PIANTA_TEMPO_BASSO,
		TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D pos) {
	return new AbstractAzione[0];
    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
