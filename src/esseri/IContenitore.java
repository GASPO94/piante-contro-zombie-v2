package esseri;

/**
 * Interfaccia per i contenitori: le zolle, i vasi, le ninfee..
 * 
 * @author Martino De Simoni
 *
 */
/*
 * Una soluzione per il metodo faiRobe delle PiantaContenitrice pu� essere:
 * 
 * faiRobe(args){ this.getContainedBeing().faiRobe(args);}
 * 
 */
public interface IContenitore {

    /**
     * Restituisce true se questo contenitore pu� contenere un certo essere senza ricorrere ai contenitori che contiene
     * @param e
     * @return
     */
     boolean canThisInParticularContain(IEssere e);
     /**
      * Restituisce true se questo contenitore pu� contenere un certo essere anche ricorrendo ai contenitori che contiene
      * @param e
      * @return
      */
     boolean canContain(IEssere e);
     
     boolean setPea(Pallino p);
     boolean setBeing(IEssere e);   

     boolean hasPea();
     boolean isOccupiedByBeing();

     IEssere getContainedBeing();
     Pallino getContainedPea();

     boolean isEmpty();
}
