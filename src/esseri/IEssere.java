
package esseri;

import java.awt.image.BufferedImage;

/**
 * Interfaccia dell'astratta Essere
 * 
 * @author Martino
 * 
 */
public interface IEssere {

    BufferedImage getImg();

    TipoEssere getTipoEssere();

    TipoTerreno getTerreno();

    void prendiDanno(double d);

    void eseguireAllaMorte(); // principalmente, effettua l'animazione

    int getTempoRichiesto();

    int getTempoTrascorso();

    boolean isDead();

    double getLife();
    
    AbstractAzione[] inAzione(int tempoTrascorso, Posizione2D pos);
    
}
