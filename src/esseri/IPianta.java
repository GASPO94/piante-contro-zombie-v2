package esseri;

public interface IPianta extends IEssere {

    /**
     * @return the plants current life
     */
    double getLife();

    int getSoliRichiesti();

    TipoTerreno getTerrenoAccettabile();

}
