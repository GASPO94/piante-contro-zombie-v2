/**
 * 
 */
package esseri;

/**
 * @author Andrea
 * 
 *         Lattuga ghiaccio � una consumabile , pi� precisamente non fa nulla
 *         quando viene giocata, infatti in faiRobe se non � attaccata ho
 *         implementato nessunAzione, ma nel momento in cui viene attaccata ,
 *         crea un piccolo danno allo zombie
 *
 */
public class LattugaGhiaccio extends AbstractConsumabile {

    private static String imgFilePath = "lattugaghiaccio.jpe";
    public final static int COSTO = 50;

    public LattugaGhiaccio() {

	super(imgFilePath, Utility.PIANTA_VITA_ALTA, Utility.PIANTA_DANNO_NULLO,
		TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	   final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {

	    if (hoAttaccato) {
		return new AbstractAzione[] { new Attacco(TipoEssere.ZOMBIE,
			Utility.QUADRETTO_CORRENTE, this.life,
			NessunAzione.getInstance(), posizione) };
	    } else {
		hoAttaccato = true;
		return new AbstractAzione[] { new Attacco(TipoEssere.ZOMBIE,
			Utility.QUADRETTO_CORRENTE, danno,
			NessunAzione.getInstance(), posizione) };
	    }
	}
	return new AbstractAzione[0];
    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {
	return this.life <= 0;
    }

}
