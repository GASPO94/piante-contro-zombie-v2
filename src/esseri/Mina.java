/**
 * 
 */
package esseri;

/**
 * @author Andrea
 *         Mina fa parte delle piante di supporto. Quando � giocata sul terreno,
 *         e non viene attaccata non fa nulla,
 *         ma nel momento in cui lo zombi attacca la Mina, questa fa danni
 *         elevati nell'area limitrofa come viene indicato
 *         nel metodo faiRobe
 *
 */
public class Mina extends AbstractSupport {

    private static String imgFilePath = "mina.jpe";
    public final static int COSTO = 25;

    public Mina() {

	super(imgFilePath, Utility.PIANTA_VITA_MINIMA,
		Utility.PIANTA_DANNO_ALTO, Utility.PIANTA_TEMPO_ALTO,
		TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {

	    if (hoAttaccato) {
		final AbstractAzione suicidio = new Attacco(TipoEssere.PIANTA,
			Utility.QUADRETTO_CORRENTE, this.life,
			NessunAzione.getInstance(), posizione);
		return new AbstractAzione[] { suicidio };
	    } else {
		hoAttaccato = true;
		return new AbstractAzione[] {
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DW_SX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_SX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP_DX,
				danno, NessunAzione.getInstance(), posizione),
			new Attacco(TipoEssere.ZOMBIE, Utility.QUADRETTO_UP_SX,
				danno, NessunAzione.getInstance(), posizione) };
	    }
	}
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return hoAttaccato;
    }

}
