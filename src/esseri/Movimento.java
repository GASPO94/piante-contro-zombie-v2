package esseri;

/**
 * 
 * Classe per comunicare al controller un movimento dell'oggetto che comunica.
 *
 * @author Martino De Simoni
 *
 */

public class Movimento extends AbstractAzione {

    public final Posizione2D vettoreMovimento;
    public final Posizione2D posizioneDiChiSiMuove;
    public final Posizione2D doveSpostarsi;
    public final IEssere chiSiMuove;

    public Movimento(final Posizione2D vettoreMovimento,
	    final Posizione2D posizioneDiChiSiMuove,
	    final IEssere chiSiMuoveArg,
	    final AbstractAzione incasodifallimento) {

	super(incasodifallimento);

	this.posizioneDiChiSiMuove = posizioneDiChiSiMuove;

	this.vettoreMovimento = vettoreMovimento;

	this.doveSpostarsi = vettoreMovimento
		.sumPositions(posizioneDiChiSiMuove);

	this.chiSiMuove = chiSiMuoveArg;
    }

}
