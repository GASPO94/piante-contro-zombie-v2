package esseri;
/**
 * 
 * Azione che segnala il movimento del Pallino
 * @author Martin
 *
 */
public class MovimentoPallino extends Movimento {

    public MovimentoPallino(final Posizione2D vettoreMovimento,
	    final Posizione2D posizioneDiChiSiMuove, final Pallino chiSiMuoveArg,
	    final AbstractAzione incasodifallimento) {
	super(vettoreMovimento, posizioneDiChiSiMuove, chiSiMuoveArg,
		incasodifallimento);
    }

}
