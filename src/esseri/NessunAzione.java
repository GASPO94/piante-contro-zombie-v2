package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 */

final public class NessunAzione extends AbstractAzione {

    private static final NessunAzione SINGLETON = new NessunAzione(); // pattern
								      // Singleton

    public static NessunAzione getInstance() {

	return SINGLETON;

    }

    private NessunAzione() {
    }

}
