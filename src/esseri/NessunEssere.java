package esseri;

import gui.NessunImmagine;

import java.awt.image.BufferedImage;

final public class NessunEssere implements IEssere {

    private final static NessunEssere SINGLETON = new NessunEssere();

    private NessunEssere() {
    };

    public static NessunEssere getInstance() {

	return SINGLETON;

    }

    @Override
    public TipoEssere getTipoEssere() {
	return null;
    }

    @Override
    public TipoTerreno getTerreno() {
	return null;
    }

    @Override
    public void prendiDanno(final double danno) {

    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public int getTempoRichiesto() {
	return 0;
    }

    @Override
    public int getTempoTrascorso() {
	return 0;
    }

    @Override
    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D pos) {
	return new AbstractAzione[0];
    }

    @Override
    public BufferedImage getImg() {
	return NessunImmagine.getInstance();
    }

    @Override
    public boolean isDead() {
	return false;
    }

    @Override
    public double getLife() {
	return 0;
    }

}
