package esseri;

import java.awt.image.BufferedImage;

public class Ninfea extends Pianta implements IContenitore {

    private static String imgFilePath = "lilypad.png";
    private boolean occupied;
    protected Pallino pallinoContenuto = null;
    protected IEssere essereContenuto = NessunEssere.getInstance();
    public final static int COSTO = 50;

    public Ninfea() {
	super(imgFilePath, Utility.PIANTA_VITA_BASSA,
		Utility.PIANTA_DANNO_NULLO, Utility.PIANTA_TEMPO_MINIMO,
		TipoTerreno.ACQUA,COSTO);
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D pos) {
	if (isOccupiedByBeing()) {
	    return essereContenuto.inAzione(tempoTrascorso, pos);
	}
	return new AbstractAzione[0];
    }

    @Override
    public boolean canThisInParticularContain(final IEssere e) {
	return     (e.getTipoEssere() == TipoEssere.PIANTA && isEmpty() )
	       || (!this.hasPea() && e.getTipoEssere()==TipoEssere.PALLINO);
    }

    @Override
    public boolean canContain(final IEssere e) {
	if (this.canThisInParticularContain(e)) {
	    return true; // zolla libera
	}

	if (essereContenuto instanceof IContenitore) {
	    return ((IContenitore) this.getContainedBeing())
		    .canThisInParticularContain(e); // zolla con un contenitore
						    // (un vaso, una ninfea..)
	}
	return false; // zolla occupata

    }

    @Override
    public boolean setBeing(final IEssere e) {
	if (canThisInParticularContain(e) && isEmpty()) {
	    essereContenuto = e;
	    occupied = true;
	    return true;
	}
	return false;
    }

    @Override
    public BufferedImage getImg() {
	if (isOccupiedByBeing()) {
	    return gui.Utility.mergeImage(super.getImg(),
		    essereContenuto.getImg());
	} else if(hasPea()){
	    return gui.Utility.mergeImage(super.getImg(),
		    pallinoContenuto.getImg());
	}
	return super.getImg();
    }

    @Override
    public IEssere getContainedBeing() {
	return essereContenuto;
    }

    @Override
    public boolean isOccupiedByBeing() {
	return occupied;
    }

    @Override
    public boolean isEmpty() {
	return !isOccupiedByBeing();
    }

    @Override
    public void prendiDanno(final double d) {
	if (isOccupiedByBeing()) {
	    essereContenuto.prendiDanno(d);
	    if (essereContenuto.isDead()) {
		essereContenuto = NessunEssere.getInstance();
		occupied = false;
	    }
	    return;
	} else {
	    super.prendiDanno(d);
	}

    }

    @Override
    public boolean setPea(final Pallino p) {
	if(this.canContain(p)){
	    this.pallinoContenuto = p;
	    return true;
	}
	return false;
    }

    @Override
    public boolean hasPea() {
	
	return pallinoContenuto!=null;
    }

    @Override
    public Pallino getContainedPea() {
	return pallinoContenuto;
    }
}
