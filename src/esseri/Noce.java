package esseri;

/**
 * Noce � una pianta che estende Difensivo, sostanzialmente non
 * fa nulla , perch� una volta giocata sul terreno deve difendere
 * dall'attaco degli zombie, quindi la vita di essa � elevata
 *
 * @author Andrea
 */
public class Noce extends AbstractDifensivo {

    private static String imgFilePath = "noce.png";
    public final static int COSTO = 50;
    public Noce() {

	super(imgFilePath, Utility.PIANTA_VITA_ALTA, Utility.PIANTA_DANNO_NULLO,
		Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D pos) {
	return new AbstractAzione[0];
    }

    @Override
    public TipoTerreno getTerreno() {
	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {
    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }
}