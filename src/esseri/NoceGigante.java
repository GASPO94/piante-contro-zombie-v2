/**
 * 
 */
package esseri;

/**
 * @author Andrea NoceGigante estende Difensivo , come faiRobe fa esattamente la
 *         stessa cosa della Noce , solo che ha una vita molto pi� alta ( per
 *         questo ho utilizzato super.life=Utility,Pianta_vita_alta *2)
 *
 */
public class NoceGigante extends AbstractDifensivo {

    private static String imgFilePath = "nocegigante.jpe";
    public final static int COSTO = 125;

    public NoceGigante() {

	super(imgFilePath, Utility.PIANTA_VITA_MOLTO_ALTA,
		Utility.PIANTA_DANNO_NULLO, Utility.PIANTA_TEMPO_BASSO,
		TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTascorso,
	    final Posizione2D pos) {
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
