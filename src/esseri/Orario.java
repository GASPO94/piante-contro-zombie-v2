package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 *
 */

public enum Orario {
    GIORNO, NOTTE
}