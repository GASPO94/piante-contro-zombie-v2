/**
 * 
 */
package esseri;

/**
 * 
 *  Pallino � un essere . in fai robe viene indicato l'attacco ,
 *  successivamente il pallino sar� creato dalle sparasemi
 *
 *  @author Andrea
 */
// Come pianta e zombie, pallino � un essere
public class Pallino extends AbstractEssere implements RicevitoreDiFeedback {

    private static String imgFilePath = "imgPiante/pallinoTrasparente.png";
    private boolean attaccato;

    public Pallino() {

	super(imgFilePath, TipoEssere.PALLINO, Utility.PIANTA_VITA_MINIMA,
		Utility.PIANTA_DANNO_BASSO, Utility.PIANTA_TEMPO__MOLTO_BASSO,
		TipoTerreno.ARIA);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    if (attaccato) {
		final Attacco suicidio = new Attacco(TipoEssere.PALLINO,
			Utility.QUADRETTO_CORRENTE, this.life,
			NessunAzione.getInstance(), posizione);
		return new AbstractAzione[] { suicidio };
	    } else {
		final MovimentoPallino seELibero = new MovimentoPallino(Utility.QUADRETTO_DX,
			posizione, this, NessunAzione.getInstance());
		final AttaccoConFeedback azioneDaImmettere = new AttaccoConFeedback(
			this, TipoEssere.ZOMBIE, Utility.QUADRETTO_DX,
			this.danno, seELibero, posizione);

		return new AbstractAzione[] { azioneDaImmettere };
	    }
	}
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return this.terreno;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {
	return life <= 0;

    }

    @Override
    public void prendiDanno(final double danno) {

	this.life -= danno;

    }

    @Override
    public void riceviFeedback(final boolean attaccoConSuccesso) {
	if (attaccoConSuccesso) {
	    attaccato = true;
	    
	}
	
    }

}
