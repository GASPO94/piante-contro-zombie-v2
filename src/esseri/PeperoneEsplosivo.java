/**
 * 
 */
package esseri;

/**
 * 
 * Peperone esplosivo colpisce gli zombi che si trovano sulla riga della
 * matrice in cui questa pianta viene posizionata. Non colpisce tutta la
 * riga , ma lo spazio indicato nel metodo faiRobe
 * 
 * @author Andrea
 */
public class PeperoneEsplosivo extends AbstractConsumabile {

    private static String imgFilePath = "peperoncinoesplosivoTrasparente.png";
    public final static int COSTO = 125;

    public PeperoneEsplosivo() {

	super(imgFilePath, Utility.PIANTA_VITA_MINIMA,
		Utility.PIANTA_DANNO_ALTO, TipoTerreno.CORTILE,COSTO);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    if (hoAttaccato) {
		return new AbstractAzione[] { new Attacco(TipoEssere.PIANTA,
			Utility.QUADRETTO_CORRENTE, this.life,
			NessunAzione.getInstance(), posizione) };
	    } else {
		AbstractAzione[] azioni = new AbstractAzione[controller.GameController
			.getCols()];
		for (int i = 0; i < controller.GameController.getCols(); i++) {

		    azioni[i] = new Attacco(TipoEssere.ZOMBIE,
			    new Posizione2D(i, 0), danno,
			    NessunAzione.getInstance(), posizione);

		}
		hoAttaccato = true;
		return azioni;
	    }
	}
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

}
