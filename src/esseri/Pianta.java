package esseri;

/**
 * @author Andrea Pianta � un essere come Pallino e Zombie ,� un'astratta che
 *         contiene i metodi utili a tutte le piante del programma.
 * 
 */
public abstract class Pianta extends AbstractEssere implements IPianta {

    // public double life;
    public int tempoInMs;
    private final int costo; // costo della piante
    private TipoTerreno terra;
    protected static final String CARTELLA_IMMAGINI_PIANTE = "imgPiante/"; // infallibile

    public TipoEssere tipo = TipoEssere.PIANTA;

    public Pianta(final String path, final Double life, final Double danno,
	    final Integer tempoRichiesto, final TipoTerreno terreno, final int costo) {
	
	super(CARTELLA_IMMAGINI_PIANTE + path, TipoEssere.PIANTA, life, danno,
		tempoRichiesto, terreno);
	this.costo = costo;

    }

    public TipoTerreno getTerrenoAccettabile() {
	return this.terra;
    }
   @Override
    public final int getSoliRichiesti(){
	return costo;
    }

    public void prendiDanno(final double d) {

	super.life -= d;
    }

    public boolean isDead() {

	return this.getLife() <= 0;
    }

    protected void cambiaImmagine(final String path) {
	super.cambiaImmagine(CARTELLA_IMMAGINI_PIANTE + path);
    }
}
