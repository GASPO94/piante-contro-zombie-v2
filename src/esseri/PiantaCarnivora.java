package esseri;

public class PiantaCarnivora extends Pianta implements RicevitoreDiFeedback {

    private static String imgFilePathHungry = "piantaCarnivoraAffamata.png";
    private static String imgFilePathSatiated = "piantaCarnivoraSazia.png";
    public final static int COSTO = 200;

    private boolean affamata = true;
    private final static int TEMPO_ATTESA = 10000;
    private static int tempoReattivit� = Utility.PIANTA_TEMPO_MINIMO;

    public PiantaCarnivora() {

	super(imgFilePathHungry, Utility.PIANTA_VITA_BASSA,
		Utility.PIANTA_DANNO_LETALE, tempoReattivit�,
		TipoTerreno.CORTILE,COSTO);
    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    if (affamata) {
		final Posizione2D dueQuadrettiVersoDestra = Utility.QUADRETTO_DX
			.sumPositions(Utility.QUADRETTO_DX);
		final AttaccoConFeedback unPoPi�InL� = new AttaccoConFeedback(
			this, TipoEssere.ZOMBIE, dueQuadrettiVersoDestra, danno,
			NessunAzione.getInstance(), posizione);
		return new AbstractAzione[] { new AttaccoConFeedback(this,
			TipoEssere.ZOMBIE, Utility.QUADRETTO_DX, danno,
			unPoPi�InL�, posizione) };
	    } else {
		// Fine dell'attesa
		affamata = true; // E' tempo di un nuovo pasto
		// Ritorno allo stato iniziale
		cambiaImmagine(imgFilePathHungry);
		tempoRichiesto = tempoReattivit�;
	    }
	}
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

    @Override
    public void riceviFeedback(final boolean attaccoRiuscito) {
	if (attaccoRiuscito) { // gnam
	    affamata = false;
	    tempoRichiesto = TEMPO_ATTESA;
	    tempoTrascorso = 0;
	    cambiaImmagine(imgFilePathSatiated);
	}
    }

}
