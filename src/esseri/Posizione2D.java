package esseri;

/**
 * 
 * @author Martino De Simoni
 * @author Marco Martini
 *
 *         I campi sono double per dare la massima libert� al programmatore
 *         del controller. Consiglio l'utilizzo di int e una griglia di piante e
 *         zombie.
 *
 *
 */

/*
 * Classe che definisce la posizione di un essere e restituisce una nuova
 * posizione
 * Ho diviso con x e y perche puo capitare che uno Zombi vada indietro di e ho
 * preferito dividerle
 * 
 */
public class Posizione2D {

    private final double x;
    private final double y;

    public Posizione2D(final double x,final double y) {

	this.x = x;
	this.y = y;
    }

    public Posizione2D sumPositions(final Posizione2D pos) {

	return new Posizione2D(x + pos.x, y + pos.y);

    }

    public double getX() {

	return this.x;
    }

    public double getY() {

	return this.y;
    }

    @Override
    public String toString() {
	return "X: " + x + " Y: " + y;
    }

}
