package esseri;

import java.awt.image.BufferedImage;

/**
 * Un quadratino della griglia di gioco di Piante contro Zombie.
 * Quindi la zolla di terra, il quadrato di acqua della piscina, la porzione di
 * tetto..
 * 
 * @author Martino De Simoni
 * 
 */

public class Quadratino implements IContenitore {

    private final TipoTerreno terreno;
    protected Pallino pallinoContenuto;

    private IEssere essereContenuto = NessunEssere.getInstance();
    private final BufferedImage img;
    // Qualche filepath lo si pu� anche mettere qui, ma non � che una comodit�.
    // Altri quadratini da usare nella versione completa del gioco sono il
    // cortile di notte, la piscina e il tetto.
    // Quadratini per versioni estese sono quelli dei minigiochi

    /**
     * Il costruttore da usare per la versione completa del gioco.
     * 
     * @param terreno
     * @param filePath
     */
    public Quadratino(final TipoTerreno terreno, final BufferedImage img) {

	this.terreno = terreno;
	this.img = img;
    }

    public TipoTerreno getTerrainType() {
	return terreno;
    }

    public void makeEmpty() {

	this.essereContenuto = NessunEssere.getInstance();

    }

    public boolean setBeing(final IEssere e) {

	if (canThisInParticularContain(e)) {

	    this.essereContenuto = e;

	    return true;

	}

	if (essereContenuto instanceof IContenitore) {
	    return ((IContenitore) this.getContainedBeing()).setBeing(e);
	}
	return false;

    }

    public boolean canThisInParticularContain(final IEssere e) {

	if (e instanceof Pallino) {
	    return !hasPea();
	}
	return this.isEmpty()
		&& (e.getTerreno() == this.terreno
			|| e.getTerreno() == TipoTerreno.ARIA)
		&& !(e.getTipoEssere() == TipoEssere.ZOMBIE && this.hasPea());

    }

    public boolean canContain(final IEssere e) {

	if (this.canThisInParticularContain(e)) {
	    return true; // zolla libera
	}

	if (essereContenuto instanceof IContenitore) {
	    return ((IContenitore) this.getContainedBeing()).canContain(e);
	}

	return false; // zolla occupata

    }

    @Override
    public IEssere getContainedBeing() {
	return essereContenuto;
    }

    public boolean isEmpty() {

	return essereContenuto == NessunEssere.getInstance()
		|| essereContenuto == null;

    }

    public boolean isOccupiedByBeing() {

	return !isEmpty();

    }

    public BufferedImage getImg() {

	return this.img;
    }

    @Override
    public boolean setPea(final Pallino p) {
	if (p == null) {
	    this.pallinoContenuto = null;
	    return true;
	}
	if (this.canContain(p)) {
	    this.pallinoContenuto = p;
	    return true;
	}
	return false;
    }

    @Override
    public boolean hasPea() {
	return pallinoContenuto != null;
    }

    @Override
    public Pallino getContainedPea() {
	return pallinoContenuto;
    }

}
