
package esseri;

/**
 * @author Andrea Sparapallino estende l'astratta Offensivo , e nel metodo
 *         faiRobe , crea il pallino .
 *
 */
public class Sparapallino extends AbstractOffensivo {

    private static String imgFilePath = "sparapallino.png";
    public final static int COSTO = 100;

    public Sparapallino() {

	super(imgFilePath, Utility.PIANTA_VITA_BASSA,
		Utility.PIANTA_DANNO_NULLO, Utility.PIANTA_TEMPO_ALTO,
		TipoTerreno.CORTILE,COSTO);
    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    return new AbstractAzione[] {
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX) };

	}
	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
