/**
 * 
 */
package esseri;

/**
 * Questa classe sostanzialmente svolge gli stessi compiti di
 * Sparapallino , solo che crea due pallini
 *
 * @author Andrea
 */
public class SparapallinoDoppio extends AbstractOffensivo {

    private static String imgFilePath = "sparapallinodoppio.jpe";
    public final static int COSTO = 200;

    public SparapallinoDoppio() {

	super(imgFilePath, Utility.PIANTA_VITA_BASSA,
		Utility.PIANTA_DANNO_NULLO, Utility.PIANTA_TEMPO_BASSO,
		TipoTerreno.CORTILE,COSTO);
    }

    public AbstractAzione[] inAzione(int tempoTrascorso,
	    Posizione2D posizione) {

	if (canAct(Utility.PIANTA_TEMPO_BASSO)) {
	    return new AbstractAzione[] {
		    //TODO Buggy
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX),
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX) };
	}

	return new AbstractAzione[0];

    }

    @Override
    public TipoTerreno getTerreno() {

	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
