/**
 * 
 */
package esseri;

/**
 * @author Andrea Questa classe svolge gli stessi compiti di SparapallinoDoppio
 *         , ma ne crea un terzo
 */
public class SparapallinoTriplo extends AbstractOffensivo {

    private static String imgFilePath = "sparapallinotriplo.jpe";
    public final static int COSTO = 400;

    public SparapallinoTriplo() {

	super(imgFilePath, Utility.PIANTA_VITA_BASSA, 0.0,
		Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE,COSTO);
    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(Utility.PIANTA_TEMPO_BASSO)) {
	    return new AbstractAzione[] {
		    //TODO Buggy
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX),
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX),
		    new Creazione(new Pallino(), NessunAzione.getInstance(),
			    posizione, Utility.QUADRETTO_DX) };

	}
	return new AbstractAzione[0];
    }


    @Override
    public TipoTerreno getTerreno() {
	return TipoTerreno.CORTILE;
    }

    @Override
    public void eseguireAllaMorte() {

    }

    @Override
    public boolean isDead() {

	return this.life <= 0;
    }

}
