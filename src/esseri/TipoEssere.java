package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 *         La classe facilita di molto la programmazione.
 *
 */

public enum TipoEssere {
    PIANTA, ZOMBIE, PALLINO, OGGETTO // Per com'� descritto il progetto, mi
				     // riesce difficile immaginare come andare
				     // oltre a questi 4 elementi ( Piante,
				     // Pallini, Zombie, Oggetti). Mi intriga la
				     // possibilit� di utilizzare pi� elementi,
				     // creare pi� alberi di esseri. Lo segnalo
				     // al game designer.
}
