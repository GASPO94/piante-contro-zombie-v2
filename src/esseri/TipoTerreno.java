package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 */

public enum TipoTerreno {
    CORTILE, ACQUA, TETTO, ARIA
}