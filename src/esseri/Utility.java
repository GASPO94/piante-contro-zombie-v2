package esseri;

/**
 * 
 * @author Martino De Simoni
 * 
 *         La seguente Ä� una classettona di costanti public final static e di
 *         metodi static vari che possono essere usate, anche solo
 *         idealmente, in DUE O PIU' classi.
 * 
 *         La classe Ä� ottima per il debug, la stesura del codice e per la
 *         traduzione da lingua a lingua.
 * 
 *         L'utilizzo di questa classe espone il programma a due problemi:
 * 
 *         - A tempo di run, un oggetto A potrebbe utilizzare un oggetto X qui
 *         definito, e B dovrebbe utilizzare lo stesso oggetto di A,
 *         cambiando oggetto in uso al variare dell'oggetto di A, mentre invece
 *         utilizza lo stesso oggetto della classe utility.
 *         Nel caso A e B non cambino oggetto, il programmatore che mantiene il
 *         codice, dovendo cambiare solo quello di A o solo di B,
 *         dovrÅ• fare attenzione.
 * 
 *         - Durante il mantenimento, nel caso in cui la stessa immagine abbia
 *         due funzioni diverso ( per esempio, usare il logo anche
 *         come sfondo), utilizzando la stessa etichetta "logo", nel momento in
 *         cui si vorrÅ• cambiare lo sfondo, si cambierÅ• anche il
 *         logo. Un po' come associare il contatore di un ciclo e quello dei
 *         thread utilizzati allo stesso int. Le variabili sono
 *         definite per semantica e non per valore.
 *
 *         In un ambiente di buona programmazione mi sento di raccomandare
 *         questa classe.
 *
 *         A mio avviso, Ä� meglio mettere qui le costanti relative alle piante
 *         e agli zombie, piÅ¯ altre entitÅ• che potrebbero in seguito essere
 *         aggiunte, piuttosto che nelle relative astratte, per semplicitÅ• di
 *         programmazione e per avere un confronto rapido sui cambiamenti in
 *         corso. Inoltre, le prestazioni sono le medesime.
 *
 */

public final class Utility {

    public final static int TRENTOTTO = 38; // usare con cura.
    // Vita e danno
    // Piante

    public final static double PIANTA_VITA_MINIMA = 1;
    public final static double PIANTA_VITA_BASSA = 20;
    public final static double PIANTA_VITA_MEDIA = 30;
    public final static double PIANTA_VITA_ALTA = 120;
    public final static double PIANTA_VITA_MOLTO_ALTA = 240;

    public final static double PIANTA_DANNO_NULLO = 0;

    public final static double PIANTA_DANNO_BASSO = 3;
    public final static double PIANTA_DANNO_MEDIO = 6;
    public final static double PIANTA_DANNO_ALTO = 10;
    public final static double PIANTA_DANNO_LETALE = Double.MAX_VALUE;

    // Zombie

    public final static double ZOMBIE_VITA_BASSA = 10;
    public final static double ZOMBIE_VITA_MEDIA = 20;
    public final static double ZOMBIE_VITA_ALTA = 30;

    public final static double ZOMBIE_DANNO_BASSO = 10;
    public final static double ZOMBIE_DANNO_MEDIO = 20;
    public final static double ZOMBIE_DANNO_ALTA = 30;

    // tempo in ms

    // Zombie
    public final static int ZOMBIE_TEMPO_MINIMO = 10;

    public final static int ZOMBIE_TEMPO_BASSO = 2000;
    public final static int ZOMBIE_TEMPO_MEDIO = 3000;
    public final static int ZOMBIE_TEMPO_ALTO = 4000;

    // Piante
    public final static int PIANTA_TEMPO_MINIMO = 10;
    public final static int PIANTA_TEMPO__MOLTO_BASSO = 200;

    public final static int PIANTA_TEMPO_BASSO = 500;
    public final static int PIANTA_TEMPO_MEDIO = 2000;
    public final static int PIANTA_TEMPO_ALTO = 3000;

    public final static Posizione2D QUADRETTO_SX = new Posizione2D(-1, 0);
    public final static Posizione2D QUADRETTO_DX = new Posizione2D(1, 0);
    public final static Posizione2D QUADRETTO_UP = new Posizione2D(0, 1);
    public final static Posizione2D QUADRETTO_DW = new Posizione2D(0, -1);
    public final static Posizione2D QUADRETTO_UP_SX = new Posizione2D(-1, 1);
    public final static Posizione2D QUADRETTO_DW_SX = new Posizione2D(-1, -1);
    public final static Posizione2D QUADRETTO_UP_DX = new Posizione2D(1, 1);
    public final static Posizione2D QUADRETTO_DW_DX = new Posizione2D(1, -1);

    public final static Posizione2D QUADRETTO_CORRENTE = new Posizione2D(0, 0);

    private Utility() {
    };

}
