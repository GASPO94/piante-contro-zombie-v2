
package esseri;

/*
 * Lo zombi ballerino viene evocato in gruppi di 4 quando entra in gioco disco zombi
 * ed è solo un po piu veloce di uno zombi normale.
 * 
 */
/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiBallerino extends AbstractZombi {

    private static String imgFilePath = "zombi_ballerino.jpeg";

    public ZombiBallerino() {

	super(imgFilePath, Utility.ZOMBIE_VITA_MEDIA,
		Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_MEDIO,
		TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];

    }

}
