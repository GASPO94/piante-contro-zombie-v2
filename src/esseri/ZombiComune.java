package esseri;

/**
 * 
 * @author Martino De Simoni
 *
 */

public class ZombiComune extends AbstractZombi {

    private static String imgFilePath = "zombieComuneTrasparente.png";

    public ZombiComune() {

	super(imgFilePath, Utility.ZOMBIE_VITA_BASSA,
		Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO,
		TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {
	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];

    }

}
