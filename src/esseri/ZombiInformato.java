package esseri;

/*
 * Lo zombi infomato è uno zombi inc cui inizialmente i danni li prende l'oggetto che porta con se
 * quando la vita dell'oggetto è uguale a 0 lo zombi informato si arrabbia e 
 * si muove molte piu velocente di prima. 
 * 
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiInformato extends AbstractZombi {

    private static String imgFilePath = "zombi_informato.jpeg";

    private double lifeOggetto = 30.0;
    private final int tempoDaArrabbiato = Utility.ZOMBIE_TEMPO_BASSO / 2;

    public ZombiInformato() {

	super(imgFilePath, Utility.ZOMBIE_VITA_BASSA,
		Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO,
		TipoTerreno.CORTILE);

    }

    /*
     * Override della funzione prendi danno perchè lo zombi infomato
     * inizialmente il danno lo prende l'oggetto
     * e solo quando la vita dell'oggetto è uguale a 0 il danno lo prende lo
     * zombie.
     */
    @Override
    public void prendiDanno(final double danno) {

	if (this.lifeOggetto != 0) {

	    this.lifeOggetto -= danno;
	    if (lifeOggetto <= 0) {
		this.tempoRichiesto = tempoDaArrabbiato;
	    }

	}

	super.prendiDanno(danno);

    }

    public double getLifeOggetto() {

	return this.lifeOggetto;
    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (this.canAct(tempoTrascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };

	}
	return new AbstractAzione[0];
    }

}
