
package esseri;

/*
 * Lo zombiPorta porta con se una enorme porta che lo difende dai colpi iniziali
 * fino a quando la vita della porta non scende a 0.
 * 
 *
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiPorta extends AbstractZombi {

    private static String imgFilePath = "zombi_porta.jpeg";

    private double lifeOggetto = Utility.ZOMBIE_VITA_ALTA;

    public ZombiPorta() {

	super(imgFilePath, Utility.ZOMBIE_VITA_MEDIA,
		Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_MEDIO,
		TipoTerreno.CORTILE);

    }

    /*
     * Override della funzione prendi danno perchè lo zombi infomato
     * inizialmente il danno lo prende l'oggetto
     * e solo quando la vita dell'oggetto è uguale a 0 il danno lo prende lo
     * zombie.
     */
    @Override
    public void prendiDanno(final double danno) {

	if (this.lifeOggetto != 0) {

	    this.lifeOggetto -= danno;

	}

	super.prendiDanno(danno);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (this.canAct(tempoTrascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];
    }

    public double getLifeOggetto() {

	return this.lifeOggetto;
    }

}
