package esseri;

/*
 * Lo zombi regbista è lo zombi piu forte e resistente della nostra applicazione.
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiRegbista extends AbstractZombi {

    private static String imgFilePath = "zombi_regbista.jpeg";

    public ZombiRegbista() {

	super(imgFilePath, Utility.ZOMBIE_VITA_ALTA, Utility.ZOMBIE_DANNO_ALTA,
		Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];
    }

}
