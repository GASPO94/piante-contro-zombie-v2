
package esseri;

/*
 * Lo zombiSecchione ha la peculiarità di essere piu forte di uno zombi normale e piu resistente
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiSecchione extends AbstractZombi {

    private static String imgFilePath = "zombi_secchione.jpeg";

    private double lifeOggetto = 3 * this.getLife();

    @Override
    public void prendiDanno(final double danno) {

	if (this.lifeOggetto != 0) {

	    this.lifeOggetto -= danno;

	}

	super.prendiDanno(danno);

    }

    public ZombiSecchione() {

	super(imgFilePath, Utility.ZOMBIE_VITA_MEDIA,
		Utility.ZOMBIE_DANNO_MEDIO, Utility.ZOMBIE_TEMPO_MEDIO,
		TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempo_trascorso,
	    final Posizione2D posizione) {

	if (canAct(tempo_trascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];
    }

}
