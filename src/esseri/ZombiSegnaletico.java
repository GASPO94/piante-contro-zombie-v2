package esseri;

/*
 * Lo zombi segnaletico ha un cono sopra la testa che lo rende due volte piu resistente dello zombi comune
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiSegnaletico extends AbstractZombi {

    private static String imgFilePath = "zombi_segnaletico.jpeg";

    private double lifeOggetto = this.getLife();

    @Override
    public void prendiDanno(final double danno) {

	if (this.lifeOggetto != 0) {

	    this.lifeOggetto -= danno;
	    if (lifeOggetto <= 0) {
		this.tempoRichiesto /= 2;
	    }

	}

	super.prendiDanno(danno);

    }

    public ZombiSegnaletico() {

	super(imgFilePath, Utility.ZOMBIE_VITA_ALTA, Utility.ZOMBIE_DANNO_BASSO,
		Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);
	    return new AbstractAzione[] { azioneDaImmettere };

	}
	return new AbstractAzione[0];

    }

}
