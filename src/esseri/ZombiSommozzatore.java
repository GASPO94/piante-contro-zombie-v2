
package esseri;

/*
 * Zombi sommozzatore puo stare solo in un terreno in cui ce l'acqua ed è piu forte di uno zombi comune
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */

public class ZombiSommozzatore extends AbstractZombi {

    private static String imgFilePath = "zombieSommozzatoreTrasparente.png";

    public ZombiSommozzatore() {

	super(imgFilePath, Utility.ZOMBIE_VITA_MEDIA,
		Utility.ZOMBIE_DANNO_MEDIO, Utility.ZOMBIE_TEMPO_BASSO,
		TipoTerreno.ACQUA);

    }

    public AbstractAzione[] inAzione(final int tempoTrascorso,
	    final Posizione2D posizione) {

	if (canAct(tempoTrascorso)) {

	    final Movimento seELibero = new Movimento(Utility.QUADRETTO_SX,
		    posizione, this, NessunAzione.getInstance());
	    final Attacco azioneDaImmettere = new Attacco(TipoEssere.PIANTA,
		    Utility.QUADRETTO_SX, this.danno, seELibero, posizione);

	    return new AbstractAzione[] { azioneDaImmettere };
	}
	return new AbstractAzione[0];

    }

}
