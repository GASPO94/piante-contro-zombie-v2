package filemanager;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Gestore di dati su file.
 * 
 * La filosofia di questa classe � "un fileManager, un file"
 *
 * @author Martino De Simoni
 *
 */

public abstract class AbstractFileManager {

    final protected File file;

    public AbstractFileManager(final String fileName) {

	this.file = new File(fileName);

    }

    /**
     * Trasforma un file di testo in array di stringhe, contando lo spazio e l'a
     * capo i blank fra parola e parola.
     * 
     * Questo rende l'a capo uno spazio con diversa leggibilit�.
     * Non usare l'a capo per dare significato in fase di aggiunta/modifica,
     * usare le parole.
     * 
     * Tecnicamente andrebbe un argomento con i blanks del file ma vabb�.
     * 
     * @return Contenuto del file di testo in formato String []
     */
    protected String[] fileToStringArray() {

	if (!file.exists()) {
	    return new String[0];
	}

	FileReader fr = null;

	try {

	    fr = new FileReader(file);
	    final char[] in = new char[(int) file.length()];
	    fr.read(in);

	    final String s = new String(in);

	    final String z = s.replace('\n', ' ').replace('\r', ' ')
		    .replace("  ", " ");

	    return z.split(" ");
	} catch (IOException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    try {
		fr.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}

    }

}
