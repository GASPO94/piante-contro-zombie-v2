package filemanager;

/**
 * Descrizione dell'entrata di uno zombie nell'interfaccia di gioco.
 * 
 * @author Martino De Simoni
 *
 */

public class EntrataZombie {

    public int tempoInMs; // a prova di idiota
    public String nomeZombie;
    public int laneId; // intuitivamente, la lane 1 sar� la prima lane a partire
		       // dall'alto.

    /**
     * 
     * @param t
     *            tempo di entrata in ms
     * @param n
     *            nome della classe dello zombie
     * @param l
     *            nome della lane in cui lo zombie comparir�
     */
    public EntrataZombie(final int t, final String n, final int l) {

	this.tempoInMs = t;
	this.nomeZombie = n;
	this.laneId = l;

    }

}
