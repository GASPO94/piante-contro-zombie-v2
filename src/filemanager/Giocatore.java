package filemanager;

import java.util.Set;

import controller.Utility;

/**
 *
 * Classe per i dati di ogni utente.
 *
 * @author Martino De Simoni
 */

public class Giocatore {

    private final String nome;
    private int soldi;
    private String livello = Utility.INITIAL_LEVEL;
    private final String plantsStringForm;
    public Set<String> pianteSbloccate = Utility.DEFAULT_PLANTS;

    public String getName() {
	return nome;
    }

    public int getMoney() {
	return soldi;
    }

    public void setMoney(final int newMoney) {
	soldi = newMoney;
    }

    public String getLevel() {
	return livello;
    }

    public void setLevel(final String newLevel) {
	this.livello = newLevel;
    }

    /**
     * 
     * @param Nome
     *            Nome del giocatore
     */

    public Giocatore(final String nome) {

	this.nome = nome;
	plantsStringForm = Utility
		.stringArrayToString(pianteSbloccate.toArray(new String[0]));
    }

    // Una mano santa in fase di debug
    /**
     * @return Rappresentazione del giocatore come stringa.
     */

    public String toString() {

	return "Nome:\t" + this.nome + "\tLivello:\t" + this.livello
		+ "\tSoldi:\t" + this.soldi + "\tPiante:\t"
		+ this.plantsStringForm;

    }

}
