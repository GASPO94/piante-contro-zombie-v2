package filemanager;

import java.util.Map;

public interface LevelReader {

    /**
     * @return La descrizione di ogni livello, presa dal file associata al
     *         LevelReader.
     */

    Map<String, Livello> leggiDatiLivelli();

}
