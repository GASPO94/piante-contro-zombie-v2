package filemanager;

import java.util.ArrayList;
import java.util.List;

import controller.Utility;

/**
 * 
 * I dati di un livello.
 * 
 * @author Martino De Simoni
 * 
 */
/*
 * Andrebbe tutto final, ma la lettura/scrittura su file la lascio il pi�
 * possibile C-like.
 */
public class Livello {

    public final List<EntrataZombie> zombie = new ArrayList<>();
    // Non final per facilitare la lettura da file
    public String nome;
    public Integer ricompensaInDenaro;
    public Integer numeroZombie;
    // Tecnicamente nelle ricompense ci possono essere anche altre piante, e
    // forse anche altre cose ma non ricordo il gioco. Mi fermo qui per non
    // sforare ancora di pi� il tempo di programmazione stabilito, ma lascio una
    // porta aperta ad altri campi.

    // Una mano santa in fase di debug
    /**
     * @return Rappresentazione del giocatore come stringa.
     */
    public String toString() {

	return "Nome:\t" + this.nome + "\tRicompensa in denaro:\t"
		+ this.ricompensaInDenaro + "\tZombie:\t"
		+ Utility.stringArrayToString(zombie.toArray(new String[0]));

    }

}
