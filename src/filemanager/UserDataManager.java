package filemanager;

import java.util.Set;

public interface UserDataManager {

    /**
     * Appende i dati del giocatore su file.
     * 
     * @param g
     *            I dati del giocatore da salvare.
     */

    void appendiDatiGiocatore(final Giocatore g);

    /**
     * Legge i dati dei vari giocatori sui file
     * 
     * @return I dati di tutti i giocatori su file.
     */
    Set<Giocatore> leggiDatiGiocatori();

    /**
     * 
     * 
     * La complessit� del seguente metodo cresce all'aumentare degli utenti e
     * della conseguente lunghezza del file associato. Gli utenti sono, secondo
     * una mia personale esperienza, mai maggiori di 10 su pc personale.
     * 
     * @author Martino De Simoni
     * @param cancellando
     *            Nome del giocatore da rimuovere dal file
     * @return true se l'operazione � andata a buon fine, false altrimenti.
     * 
     */

    boolean cancellaDatiGiocatore(final String _cancellando);

    /**
     * Salva i dati del giocatore, sovrascrivendo se deve.
     * 
     * @param g
     *            Dati del giocatore da salvare.
     */

    void aggiornaDatiGiocatore(final Giocatore g);

    /**
     * Ritorna un giocatore da file dato il nome
     * 
     * @param _nome
     *            Nome del giocatore
     * @return Giocatore ricercato
     */
    Giocatore cercaGiocatoreInFile(final String _nome);

}
