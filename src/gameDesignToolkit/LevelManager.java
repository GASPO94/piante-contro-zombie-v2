package gameDesignToolkit;

import filemanager.LevelReaderImpl;

/**
 * 
 * Writer dei dati di un livello.
 * 
 * Non implementato per motivi di tempo.
 * 
 * @author Martino De Simoni
 *
 */

/*
 * Dritte: metodo insert(String livello, EntrataZombie) per inserire l'entrata
 * di uno zombie.
 * Ordinare le entrate con un comparator che prenda in considerazione il solo
 * tempo di entrata.
 * 
 */

public class LevelManager extends LevelReaderImpl {

    public LevelManager(String fileName) {
	super(fileName);
    }

}
