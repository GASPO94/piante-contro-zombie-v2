package gui;

import java.awt.*;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import javax.swing.JPanel;

/**
 *
 * Astratta della classe frame da utilizzare.
 *
 * @author Martino De Simoni
 *
 */

public abstract class AbstractFrame extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected JPanel inUso = new JPanel(); // non serve, ma non � null..

    /**
     * @param title
     *            the window title
     * @param lm
     *            the layoutmanager for the main panel
     */
    public AbstractFrame(final String title, final BufferedImage icon) {

	super(title);

	this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

	setLocationByPlatform(true);

	final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	final int sw = (int) screen.getWidth();
	final int sh = (int) screen.getHeight();
	setSize(sw / Utility.ORIZONTAL_RAZIO_WHEN_NOT_FULLSCREEN,
		sh / Utility.VERTICAL_RAZIO_WHEN_NOT_FULLSCREEN);
	setMinimumSize(
		new Dimension(sw / Utility.ORIZONTAL_RAZIO_WHEN_NOT_FULLSCREEN,
			sh / Utility.VERTICAL_RAZIO_WHEN_NOT_FULLSCREEN));
	if (Utility.FULLSCREEN) {
	    this.setExtendedState(JFrame.MAXIMIZED_BOTH);

	}
	setIconImage(icon);

    }

    public void setMainPanel(final JPanel newPanel, final boolean visible) {

	this.remove(inUso);
	this.inUso = newPanel;
	this.add(newPanel);
	newPanel.setSize(this.getContentPane().getSize());
	setContentPane(newPanel);
	getContentPane().setVisible(visible);

    }

    /**
     * @return the main {@link JPanel}
     */

    public JPanel getMainPanel() {
	return (JPanel) this.getContentPane().getComponent(0);
    }

}
