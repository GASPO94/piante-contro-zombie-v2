package gui;

import java.awt.MenuContainer;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.Serializable;
import java.util.List;

import javax.accessibility.Accessible;

public interface GamePanel
	extends ImageObserver, MenuContainer, Serializable, Accessible {

    void updateImmaginiPiante(final List<BufferedImage> nuoveImmagini,
	    final Integer soli);

    void setVisible(boolean b);

    void inviaMessaggioVittoria();

    void inviaMessaggioSconfitta();

}
