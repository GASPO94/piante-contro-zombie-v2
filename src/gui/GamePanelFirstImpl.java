package gui;

import java.awt.Color;

import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.IGameController;

import esseri.Posizione2D;

/**
 * 
 * Da questo pannello si gioca alla partita della campagna.
 *
 * @author Martino De Simoni
 */
/*
 * La classe implementa il pattern mvc e compone la view. La presente � una
 * classe totalmente "stupida", che definisce la sola grafica relegando al
 * controller quasi qualsiasi azione di logica.
 */

public class GamePanelFirstImpl extends JPanel implements GamePanel {

    private static final long serialVersionUID = 1L;

    private final List<BufferedImage> immaginiDelTerreno;

    private JLabel immagineConSfondo;

    private final Integer COLS;
    private final Integer ROWS;
    private final JPanel griglia;
    private final IGameController controller;
    private final List<JLabel> immaginiInGriglia;
    private final JLabel sunLabel;

    private void removeImagesFromGrid() {

	for (final JLabel l : immaginiInGriglia) {
	    griglia.remove(l);
	}

	immaginiInGriglia.clear();
    }

    // TODO in questa versione, c'� solo erba e l'erba � tutta di un
    // colore. In una versione pi� complessa, le immagini dei terreni viene
    // passata
    // in un vector al costruttore del pannello
    private void colorGrid(final List<BufferedImage> nuoveImmagini) {

	removeImagesFromGrid();

	for (int i = 0; i < nuoveImmagini.size(); i++) {

	    final BufferedImage immagineDaInserire = nuoveImmagini.get(i);

	    if (Utility.isAValidImage(immagineDaInserire)) {
		immagineConSfondo = new JLabel(new ImageIcon(Utility.mergeImage(
			immaginiDelTerreno.get(i), immagineDaInserire)));
	    } else {
		immagineConSfondo = new JLabel(
			new ImageIcon(immaginiDelTerreno.get(i)));
	    }

	    griglia.add(immagineConSfondo);
	    immaginiInGriglia.add(immagineConSfondo);

	}

	griglia.revalidate();
	griglia.repaint();

    }

    /**
     * Update del pannello di gioco.
     * 
     * @param nuoveImmagini
     *            Nuove immagini degli esseri da visualizzare
     * @param soli
     *            Soli spendibili da visualizzare
     */
    public void updateImmaginiPiante(final List<BufferedImage> nuoveImmagini,
	    final Integer soli) {

	sunLabel.setText(Integer.toString(soli));

	colorGrid(nuoveImmagini);

    }

    public GamePanelFirstImpl(final List<PlantButton> plantButtons,
	    final int cols, final int rows, final IGameController _controller,
	    final List<BufferedImage> _immaginiDelTerreno) {

	controller = _controller;
	COLS = cols;
	ROWS = rows;

	this.immaginiDelTerreno = _immaginiDelTerreno;
	immaginiInGriglia = new ArrayList<>(ROWS * COLS);

	this.setLayout(new GridLayout(2, 1));
	// Inizializzo il pannello in alto. Per chiarimenti, guardare una
	// schermata del gioco originale
	final JPanel pannelloInAlto = new JPanel();
	final int things = plantButtons.size() + 1 + 1; // Nel pannello in
						        // altoci
	// stanno nell'ordine: il
	// sole, i bottoni delle
	// piante, e il bottone
	// della pala
	pannelloInAlto.setLayout(new GridLayout(1, things));
	// Inizializzo il robo dei soli
	sunLabel = new JLabel(new ImageIcon(Utility.SOLE), JLabel.HORIZONTAL);
	pannelloInAlto.add(sunLabel);

	// Inizializzo i bottoni delle piante
	for (final PlantButton p : plantButtons) {

	    p.addActionListener(e -> {
		this.controller.clickBottone(p.getID());
	    });
	    p.setEnabled(true);
	    pannelloInAlto.add(p);
	}
	// Inizializzo il bottone della pala
	final JButton pala = new JButton();
	pala.setIcon(new ImageIcon(Utility.PALA));
	pala.setBackground(Color.orange);

	pala.addActionListener(e -> {

	    this.controller.clickPala();
	});

	pannelloInAlto.add(pala);
	// Add del pannello
	add(pannelloInAlto);

	// Griglia di sotto da aggiungere a BorderLayout.CENTER. Poi basta, si
	// va avanti cogli actionListener
	griglia = new JPanel(new GridLayout(ROWS, COLS));

	add(griglia);

	/*
	 * 
	 * se viene cliccato un bottone, notifica il controller (implementato
	 * altrove) se viene cliccata la griglia, notifica il controller
	 */

	// Notifica il controller se viene cliccata la griglia
	this.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(final MouseEvent e) {

		final Point mouse = getMousePosition();

		// Se si � fuori dai limiti, non fare niente.
		if (mouse == null || mouse.x < griglia.getX()
			|| mouse.y < griglia.getY()
			|| mouse.x > griglia.getX() + griglia.getWidth()
			|| mouse.y > griglia.getY() + griglia.getHeight()) {
		    return;
		} else {

		    // Il codice che segue sembra complicato, ma un disegno pu�
		    // aiutare molto.
		    // Dato un click, al controller viene notificata la
		    // posizione in griglia cliccata

		    controller.clickGriglia(new Posizione2D(
			    (mouse.x - griglia.getX())
				    / (griglia.getWidth() / COLS),
			    (mouse.y - griglia.getY())
				    / (griglia.getHeight() / ROWS)));
		}
	    }
	});
	this.setVisible(true);
	super.update(this.getGraphics());
	setSize(getPreferredSize());

    }

    @Override
    public void inviaMessaggioVittoria() {
	JOptionPane.showMessageDialog(this, Utility.VICTORY, "",
		JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void inviaMessaggioSconfitta() {
	JOptionPane.showMessageDialog(this, Utility.DEFEAT, "",
		JOptionPane.WARNING_MESSAGE);
    }

}
