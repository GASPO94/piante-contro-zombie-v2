package gui;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * 
 * 
 * Classe istanziabile del frame da utilizzare, preferibilmente non pi� d'uno
 * per istanza del programma.
 * 
 * @author Martino De Simoni
 */

public class MainFrame extends AbstractFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public MainFrame(final String title, final BufferedImage icon) {

	super(title, icon);

	this.setMainPanel(new JPanel(new BorderLayout()) {
	    /**
	     * 
	     */
	    private static final long serialVersionUID = 1L;

	    @Override
	    public void paintComponent(final Graphics g) {
		g.drawImage(Utility.SFONDO_IN_CARICAMENTO, 0, 0, null);
	    }
	}, true);

	/*
	 * Stralcio di codice preso online
	 */

	this.addWindowListener(new WindowAdapter() {

	    public void windowClosing(final WindowEvent we) {
		final String objButtons[] = { Utility.CLOSE, Utility.CANCEL };
		final int promptResult = JOptionPane.showOptionDialog(null,
			Utility.MESSAGE_FOR_EXIT, Utility.TITLE,
			JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
			null, objButtons, objButtons[1]);
		if (promptResult == JOptionPane.YES_OPTION) {

		    System.exit(0);
		}
	    }
	});
    }

}
