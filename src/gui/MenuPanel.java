package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.AbstractPanelController;
import filemanager.Giocatore;

/**
 * 
 *
 * Da questo pannello si decide a quale altro dei pannelli passare, come in
 * qualsiasi men� di gioco.
 *
 * @author Martino De Simoni
 */
/*
 * La classe implementa il pattern mvc e compone la view.
 * La presente � una classe totalmente "stupida", che definisce la sola grafica
 * relegando al controller quasi qualsiasi azione
 * di logica.
 *
 * Eccezion fatta per i bottoni, la classe � totalmente riutilizzabile.
 * 
 */

public class MenuPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private final Giocatore giocatore;
    private final BufferedImage background;
    // Bottoni
    private final JButton options = new JButton(Utility.OPTIONS);
    private final JButton backToUserChoice = new JButton(
	    Utility.BACK_TO_USER_CHOICE);
    private final JButton exit = new JButton(Utility.SAVE_AND_EXIT);
    private final JButton campaign;
    // Controller

    /**
     * 
     * Metodo preso da
     * http://www.simplesoft.it/background_image_per_componenti_java_swing.html
     * 
     */

    protected void paintComponent(final Graphics g) {

	super.paintComponent(g);

	g.drawImage(background, 0, 0, getWidth(), getHeight(), this);
	campaign.setText(Utility.PLAY_CAMPAIGN + " Livello "
		+ giocatore.getLevel().replace("_", " "));
    }

    public MenuPanel(final AbstractPanelController<MenuPanel> controller,
	    final Giocatore newPlayer, final BufferedImage newBackground) {

	// Inizializzazione campi

	JLabel dati = new JLabel();
	giocatore = newPlayer;

	background = newBackground;

	this.setLayout(new BorderLayout());

	final JPanel buttonsPanel = new JPanel(new FlowLayout());
	buttonsPanel.setOpaque(false);
	// Inizializzazione bottoni

	campaign = new JButton(Utility.PLAY_CAMPAIGN + " Livello "
		+ giocatore.getLevel().replace("_", " "));

	buttonsPanel.add(campaign);
	buttonsPanel.add(options);
	buttonsPanel.add(backToUserChoice);
	buttonsPanel.add(exit);

	this.add(buttonsPanel, BorderLayout.NORTH);

	campaign.addActionListener(e -> {
	    controller.notifyController(EventiPannello.SCELTA_PIANTE);
	});

	options.addActionListener(e -> {
	    controller.notifyController(EventiPannello.OPZIONI);
	});

	backToUserChoice.addActionListener(e -> {
	    controller.notifyController(EventiPannello.SCELTA_UTENTE);
	});

	exit.addActionListener(e -> {
	    controller.notifyController(EventiPannello.USCITA);
	});

	dati = new JLabel(Utility.NAME + ": " + newPlayer.getName() + "  "
		+ Utility.MONEY + ": " + newPlayer.getMoney());

	dati.setHorizontalAlignment(this.getWidth() / 2);

	this.add(dati, BorderLayout.CENTER);

    }

}
