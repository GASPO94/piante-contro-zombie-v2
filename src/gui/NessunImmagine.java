package gui;

import java.awt.image.BufferedImage;

/**
 * @author Martino De Simoni
 */
public final class NessunImmagine extends BufferedImage {

    private static final NessunImmagine NESSUN_IMMAGINE = new NessunImmagine(); // pattern
									        // Singleton

    public static NessunImmagine getInstance() {
	return NESSUN_IMMAGINE;

    }

    private NessunImmagine() {
	super(1, 1, 2);
    }

    public String toString() {

	return "no image to display"; // solo per debug => solo in inglese

    }

}
