package gui;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class PlantButton extends JButton {

    /**
     * 
     * Pulsante con l'icona di una pianta.
     * 
     * L'unico vero pulsante utilizzato nell'interfaccia di gioco, per
     * chiarimenti si pu� guardare una schermata del gioco originale.
     * 
     * @author Martino De Simoni
     * 
     */

    private static final long serialVersionUID = 1L;

    private final String id;

    public PlantButton(final Icon img, final Integer price, final String id) {

	this.setIcon((Icon) img);
	this.setVerticalAlignment(SwingConstants.BOTTOM);
	this.setBackground(Color.orange);
	this.setText(price.toString());

	this.id = id;

    }

    public String getID() {
	return id;
    }

}
