package gui;

import java.awt.Graphics;
import java.awt.MenuContainer;
import java.awt.image.ImageObserver;
import java.io.Serializable;
import java.util.Set;

import javax.accessibility.Accessible;
import javax.swing.JList;

public interface UserChoicePanel
	extends ImageObserver, MenuContainer, Serializable, Accessible {

    // Metodi per l'I/O
    Set<String> getChoices();

    // Prendere input da dialog
    String inputByDialog(final String title, final String msg);

    // Dare output da dialog
    void messageByDialog(final String msg);

    // Getter della lista di scelte
    JList<String> getList();

    // Setter della lista di scelte (l'ordine non � importante)
    void setChoices(final Set<String> choices);

    // Update
    void update(final Graphics g);

    void setVisible(boolean b);

    Graphics getGraphics();

}
